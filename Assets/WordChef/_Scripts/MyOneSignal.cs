﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MyOneSignal : MonoBehaviour
{

    private const string ONESIGNAL_ID = "c8d23489-427b-400a-8c13-ef78707d7492";
    void Start()
    {
        //		OneSignal.SetLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.VERBOSE);

        OneSignal.StartInit(ONESIGNAL_ID)
            .HandleNotificationOpened(HandleNotificationOpened)
            .HandleNotificationReceived(HandleNotificationReceived)
            .InFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            .EndInit();

        OneSignal.RegisterForPushNotifications();
        OneSignal.SetSubscription(true);
    }

    public void HandleNotificationReceived(OSNotification notification)
    {
        if (notification.payload == null || notification.payload.additionalData == null || notification.payload.additionalData.Count == 0)
        {
            return;
        }
        Dictionary<string, object> data = notification.payload.additionalData;
        // foreach (KeyValuePair<string, object> val in data)
        // {
        //     if (val.Key == AppConstants.UPDATEAPPKEYWORD && AppConstants.BUNDLEVERSION != int.Parse((string)val.Value))
        //     {
        //         Talker.Instance.PlayOnUpdateAvailable();
        //         DialogBoxCreator.Instance.CreateNewDialogBox
        //         (
        //         "New Update is available.\n Update Now?.", "Yes", "No", (btnClickType) =>
        //         {
        //             if (btnClickType == ButtonClickType.OPTION1)
        //             {
        //                 MarketURLHelper.Instance.OpenMarketUrl(AppConstants.BUNDLEIDENTIFIER);
        //             }
        //         });
        //     }
        // }
    }

    public void HandleNotificationOpened(OSNotificationOpenedResult result)
    {
        OSNotification notification = result.notification;
        if (notification != null)
        {
            HandleNotificationReceived(notification);
        }
    }
}

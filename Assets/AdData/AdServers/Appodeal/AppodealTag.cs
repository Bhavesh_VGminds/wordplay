﻿using System;
using VGMTools.Ads.Properties;
using VGMTools.Ads.Relations.Shared;

namespace AdServers
{
	namespace AppodealAds
	{
		public interface IAppodealBanner : IPartialScreenTag, IBannerTag, IHideAd, IShowAd<BannerPos>
		{

		}

		public interface IAppodealReward : IFullScreenTag, IRewardTag, IAdComplete, IShowAd<Action>, IAdReady
		{

		}

		public interface IAppodealInterstitial : IFullScreenTag, IInterstitialTag, IAdComplete, IShowAd<Action>, IAdReady
		{

		}
	}
}
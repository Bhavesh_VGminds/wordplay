﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class ButtonGotoScreen : MonoBehaviour
{


    public GameObject screen, Container;
    public bool useScreenFader = true;
    public Sprite backImage, achivementImage;

    GameObject MissionPanel = null;

    public static ButtonGotoScreen instance;

    void Awake(){
        instance=this;
    }

    void Start()
    {

        if(Prefs.IsAchivementActive)
            DOTweenManager.ButtonActiveEffect(this.transform);
        else
            DOTweenManager.DisableTween(this.transform);

            // Back();
    }

    private void OnDialogOpened()
    {
        if(this)
            this.GetComponent<Button>().interactable=false;
    }

    private void OnDialogClosed()
    {
         if(this)
            this.GetComponent<Button>().interactable=true;
    }

    public void OnClickButton()
    {
        this.GetComponentsInChildren<Image>()[1].sprite = backImage as Sprite;
        this.GetComponentsInChildren<Image>()[1].gameObject.GetComponent<RectTransform>().sizeDelta=new Vector2(60,60);
        this.GetComponentsInChildren<Image>()[1].color = new Color32(91, 191, 193, 255);
        this.GetComponent<Button>().onClick.RemoveAllListeners();
        this.GetComponent<Button>().onClick.AddListener(() => Back());
        MissionPanel = Instantiate(screen);
		this.GetComponent<Button>().onClick.AddListener(() => CUtils.ReloadScene(true));
		MissionPanel.transform.SetParent (GameObject.Find("Canvas").transform, false);
		MissionPanel.GetComponent<RectTransform>().localScale=new Vector3(1,1,1);
		MissionPanel.GetComponent<RectTransform>().localPosition=Vector3.zero;
        GameObject.FindGameObjectWithTag("MissionProfileImage").GetComponent<Image>().sprite=Resources.Load("Avatars/"+Prefs.PlayerProfileImage,typeof(Sprite)) as Sprite;
        MissionPanel.SetActive(false);
        CUtils.LoadScreen(MissionPanel, useScreenFader);

        DialogController.instance.onDialogsOpened += OnDialogOpened;
        DialogController.instance.onDialogsClosed += OnDialogClosed;

        Sound.instance.PlayButton();
      
    }
    public void Back()
    {
        this.GetComponentsInChildren<Image>()[1].sprite = achivementImage as Sprite;
        this.GetComponentsInChildren<Image>()[1].gameObject.GetComponent<RectTransform>().sizeDelta=new Vector2(90 ,90);
        this.GetComponentsInChildren<Image>()[1].color = new Color32(255, 255, 255, 255);
        this.GetComponent<Button>().onClick.RemoveAllListeners();
        this.GetComponent<Button>().onClick.AddListener(() => OnClickButton());

        Sound.instance.PlayButton();



        DialogController.instance.onDialogsOpened += OnDialogOpened;
        DialogController.instance.onDialogsClosed += OnDialogClosed;
    }   
}

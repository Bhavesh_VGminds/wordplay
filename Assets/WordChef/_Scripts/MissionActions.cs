﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionActions : MonoBehaviour
{

    public Transform MissionActive;
    void Start()
    {
        Prefs.onMissionReached += onMissionReached;
    }

    public void onMissionReached(string mission, int value)
    {
        Debug.Log("Active Mission Name :" + mission);
        switch (mission)
        {
            case "Login":
                if (PlayerPrefs.GetInt("MLogin") == value)
                {
                    Toast.instance.ShowMessage("Login Mission");
                    DOTweenManager.ButtonActiveEffect(MissionActive);
                }
                break;
            case "Levels":
                if (PlayerPrefs.GetInt("MLevels") == value)
                {
                    Toast.instance.ShowMessage("Level Mission");
                    DOTweenManager.ButtonActiveEffect(MissionActive);
                }
                break;
            case "Words":
                if (PlayerPrefs.GetInt("MWords") == value)
                {
                    Toast.instance.ShowMessage("Words Mission");
                    DOTweenManager.ButtonActiveEffect(MissionActive);
                }
                break;
            case "Hidden_Words":
                if (PlayerPrefs.GetInt("MHidden_Words") == value)
                {
                    Toast.instance.ShowMessage("Hidden Words Mission");
                    DOTweenManager.ButtonActiveEffect(MissionActive);
                }
                break;
            case "Hints":
                if (PlayerPrefs.GetInt("MHints") == value)
                {
                    Toast.instance.ShowMessage("Hint Mission");
                    DOTweenManager.ButtonActiveEffect(MissionActive);
                }
                break;
            case "Reward_Video":
                if (PlayerPrefs.GetInt("MReward_Video") == value)
                {
                    Toast.instance.ShowMessage("Reward Video Mission");
                    DOTweenManager.ButtonActiveEffect(MissionActive);
                }
                break;
            case "Purchase_Item":
                if (PlayerPrefs.GetInt("MPurchase_Item") == value)
                {
                    Toast.instance.ShowMessage("Purchase Item Mission");
                    DOTweenManager.ButtonActiveEffect(MissionActive);
                }
                break;
        }
    }


    void OnDisable()
    {
        Prefs.onMissionReached -= onMissionReached;
    }
}

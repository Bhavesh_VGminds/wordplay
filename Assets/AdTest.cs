﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VGMTools.Ads.Manager;
using VGMTools.Ads.Properties;
using VGMTools.Ads.Relations.Shared;

public class AdTest : MonoBehaviour
{

    // Use this for initialization


    public void btnBannerAds()
    {
        AdManager.ShowAd<IBannerTag, BannerPos>(BannerPos.Bottom);
    }



    public void btnInterstitialAds()
    {
        AdManager.ShowAd<IInterstitialTag, System.Action>(null);
    }

    public void rewardsomething()
    {
        AdManager.ShowAd<IRewardTag, System.Action>(CallBack);
    }

    public void btnHide()
    {
        AdManager.HideAd();
    }

    void CallBack()
    {
        DialogController.instance.ShowDialog(DialogType.RewardedVideo);
    }
}

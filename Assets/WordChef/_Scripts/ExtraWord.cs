﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using DG.Tweening;
public class ExtraWord : MonoBehaviour
{
    public List<string> extraWords = new List<string>();
    public GameObject existMessage;
    public Transform beginPoint, endPoint, Suitcase;
    public GameObject lightEffect;

    private int world, subWorld, level;
    private CanvasGroup existMessageCG;
    private bool isMessageShowing;
    private Text flyText;

    public static ExtraWord instance;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        world = Prefs.currentWorld;
        subWorld = Prefs.currentSubWorld;
        level = Prefs.currentLevel;

        extraWords = Prefs.GetExtraWords(world, subWorld, level).ToList();
        existMessage.SetActive(false);
        existMessageCG = existMessage.GetComponent<CanvasGroup>();

        UpdateUI();
    }

    private void UpdateUI()
    {
        lightEffect.SetActive(Prefs.extraProgress >= Prefs.extraTarget);
        if (Prefs.extraProgress >= Prefs.extraTarget)
        {
            lightEffect.transform.parent.GetComponent<Animator>().enabled = false;
            DOTweenManager.ButtonActiveEffect(lightEffect.transform.parent);
        }
        else
        {
            DOTweenManager.DisableTween(lightEffect.transform.parent);
            lightEffect.transform.parent.GetComponent<Animator>().enabled = true;
        }
        if (Prefs.extraProgress > 0 && !Prefs.isExtraWordOverlay)
        {
            MainController.instance.overlayExtraWord.gameObject.SetActive(true);
            MainController.instance.isOverlayActive = true;
            Prefs.isExtraWordOverlay = true;
        }
        MainController.instance.btnExtraWords.transform.Find("Panel").GetComponentInChildren<Text>().text = Prefs.extraProgress.ToString() + "/" + Prefs.extraTarget.ToString();
    }

    public void ProcessWorld(string word)
    {
        if (extraWords.Contains(word))
        {
            if (isMessageShowing) return;
            isMessageShowing = true;

            ShowMessage("");
        }
        else
        {
            var middlePoint = CUtils.GetMiddlePoint(beginPoint.position, endPoint.position, 0.4f);
            Vector3[] waypoint = { beginPoint.position, middlePoint, endPoint.position };

            flyText = Instantiate(MonoUtils.instance.letter);
            flyText.text = word;
            flyText.transform.position = beginPoint.position;
            flyText.transform.SetParent(MonoUtils.instance.textFlyTransform);
            flyText.transform.localScale = TextPreview.instance.text.transform.localScale;
            iTween.MoveTo(flyText.gameObject, iTween.Hash("path", waypoint, "time", 1f, "oncomplete", "OnTextMoveToComplete", "oncompletetarget", gameObject));
            flyText.DOFade(0, 0.3f).SetDelay(1);
            AddNewExtraWord(word);
            Sound.instance.Play(Sound.Others.ExtraWord);
        }
    }

    private void ShowMessage(string message)
    {
        existMessage.SetActive(true);
        existMessageCG.alpha = 0;
        iTween.ValueTo(gameObject, iTween.Hash("from", 0, "to", 1, "time", 0.3f, "OnUpdate", "OnMessageUpdate", "oncomplete", "OnMessageShowComplete"));
    }

    public void AddNewExtraWord(string word)
    {
        extraWords.Add(word);
        Prefs.SetExtraWords(world, subWorld, level, extraWords.ToArray());
        Prefs.extraProgress++;
        Prefs.totalExtraAdded++;
        MainController.instance.btnExtraWords.SetActive(Prefs.extraProgress > 0);
    }

    private void OnMessageUpdate(float value)
    {
        existMessageCG.alpha = value;
    }

    private void OnMessageShowComplete()
    {
        Timer.Schedule(this, 0.5f, () =>
        {
            iTween.ValueTo(gameObject, iTween.Hash("from", 1, "to", 0, "time", 0.3f, "OnUpdate", "OnMessageUpdate", "oncomplete", "OnMessageHideComplete"));
        });
    }

    private void OnMessageHideComplete()
    {
        isMessageShowing = false;
    }

    private void OnTextMoveToComplete()
    {
        Suitcase.DOShakePosition(1, 4);
        UpdateUI();
        Destroy(flyText.gameObject, 1f);
    }

    public void OnLightRotateComplete()
    {
        lightEffect.SetActive(false);
    }

    public void OnClaimed()
    {
        UpdateUI();
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using VGMTools.Ads.Manager;
using VGMTools.Ads.Properties;
using VGMTools.Ads.Relations.Shared;
public class CurrencyBallance : MonoBehaviour
{


    private void Start()
    {
        this.GetComponent<Text>().text = CurrencyController.GetBalance().ToString();
        if (!PlayerPrefs.HasKey(PrefKeys.CURRENCY))
        {
            CurrencyController.SetBalance(ConfigController.instance.GAME_CONFIG.coinConfig.beginner_pack_coins_limit);
        }
        UpdateBalance();
        CurrencyController.onBalanceChanged += OnBalanceChanged;
    }

    private void UpdateBalance()
    {
        if (Const.doScrumble)
            StartCoroutine(countTO(CurrencyController.GetBalance()));
        else
            gameObject.SetText(CurrencyController.GetBalance().ToString());
    }

    IEnumerator countTO(int target)
    {

        Text text = this.GetComponent<Text>();
        string OldValue = text.text;
        for (float timer = 0; timer < 1; timer += Time.deltaTime)
        {
            float progress = timer / 1;
            int score = (int)Mathf.Lerp(int.Parse(OldValue), target, progress);
            yield return null;
            text.text = score.ToString();
        }
        text.text = target.ToString();

        Const.doScrumble = false;


    }

    void UpdateUI()
    {
        StartCoroutine(countTO(CurrencyController.GetBalance()));
    }

    private void OnBalanceChanged()
    {
        UpdateBalance();
    }

    private void OnDestroy()
    {
        CurrencyController.onBalanceChanged -= OnBalanceChanged;
    }
}

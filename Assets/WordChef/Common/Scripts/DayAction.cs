﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class DayAction : MonoBehaviour
{
    public int dayInterval = 1;
    public string actionName = "";
    public UnityEvent onActionReached;

    public static DayAction Instance;

    private void Start()
    {
        int currentDay = (int)CUtils.GetCurrentTimeInDays();
        int day = CPlayerPrefs.GetInt("day_action_" + actionName, -1);

        if (currentDay - day >= dayInterval)
        {
            CPlayerPrefs.SetInt("day_action_" + actionName, currentDay);
            Prefs.GetTotalDaysOfLogin++;
            if (Prefs.GetTotalDaysOfLogin == 7)
                Prefs.GetTotalDaysOfLogin = 1;
            if (onActionReached != null) onActionReached.Invoke();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingDialog : Dialog
{

    public Color enabledColor;
    public Color disabledColor;

    private bool Music_Swapped = true, Sound_Swapped = true;

    public Image Music_image, Music_icon, Music_knob, Sound_image, Sound_Icon, Sound_knob;
    public Sprite Music_On, Music_Off, Sound_On, Sound_Off;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        // if (Prefs.IsMusicOn)
        // {
        //     Music_Swapped = false;
        //     SwapMusic();
        // }
        // else
        // {
        //     Music_Swapped = true;
        //     SwapMusic();
        // }

        if (Prefs.IsSoundOn)
        {
            Sound_Swapped = false;
            SwapSound();
        }
        else
        {
            Sound_Swapped = true;
            SwapSound();
        }
    }



    // public void SwapMusic()
    // {
    //     if (Music_Swapped)
    //     {
    //         Music_image.transform.parent.GetComponent<Slider>().value = 0;
    //         Music_image.color = disabledColor;
    //         Music_knob.color = new Color(disabledColor.r, disabledColor.g, disabledColor.b, 255);
    //         Music_icon.sprite = Music_Off;
    //         Music_Swapped = false;
    //     }
    //     else
    //     {
    //         Music_image.transform.parent.GetComponent<Slider>().value = 1;
    //         Music_image.color = enabledColor;
    //         Music_knob.color = new Color(enabledColor.r, enabledColor.g, enabledColor.b, 255);
    //         Music_icon.sprite = Music_On;
    //         Music_Swapped = true;
    //     }

    //     Music.instance.SetEnabled(Music_Swapped,true);
    // }

    public void SwapSound()
    {
        if (Sound_Swapped)
        {
            Sound_image.transform.parent.GetComponent<Slider>().value = 0;
            Sound_image.color = disabledColor;
            Sound_knob.color = new Color(disabledColor.r, disabledColor.g, disabledColor.b, 255);
            Sound_Icon.sprite = Sound_Off;
            Sound_Swapped = false;
        }
        else
        {
            Sound_image.color = enabledColor;
            Sound_knob.color = new Color(enabledColor.r, enabledColor.g, enabledColor.b, 255);
            Sound_image.transform.parent.GetComponent<Slider>().value = 1;
            Sound_Icon.sprite = Sound_On;
            Sound_Swapped = true;
        }

    Sound.instance.SetEnabled(Sound_Swapped);
    }

    public void btnSave()
    {
        Close();
    }


}

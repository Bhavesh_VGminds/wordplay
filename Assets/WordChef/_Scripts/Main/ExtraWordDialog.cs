﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExtraWordDialog : Dialog
{
    public Transform claimTr;
    public ExtraProgress extraProgress;
    public GameObject claimButton;
    public Text progressText;

    private int numWords, claimQuantity;

    protected override void Start()
    {
        base.Start();
        extraProgress.target = Prefs.extraTarget;
        extraProgress.current = Prefs.extraProgress;

        UpdateUI();
    }

    public void Claim()
    {
        claimQuantity = (int)extraProgress.target / 10 * 2;

        extraProgress.current -= (int)extraProgress.target;
        Prefs.extraProgress = (int)extraProgress.current;

        UpdateUI();

        StartCoroutine(ClaimEffect());
        ExtraWord.instance.OnClaimed();

        if (Prefs.extraTarget == 10 || Prefs.totalExtraAdded > 10)
        {
            Prefs.extraTarget = Prefs.extraTarget + 10;
            extraProgress.target = Prefs.extraTarget;
            UpdateUI();
        }
    }

    private IEnumerator ClaimEffect()
    {
        Transform rubyBalance = GameObject.FindWithTag("RubyBalance").transform;
        var middlePoint = CUtils.GetMiddlePoint(claimTr.position, rubyBalance.position, -0.4f);
        Vector3[] waypoints = { claimTr.position, middlePoint, rubyBalance.position };

        for (int i = 0; i < claimQuantity; i++)
        {
            GameObject gameObj = Instantiate(MonoUtils.instance.rubyFly);
            gameObj.transform.position = waypoints[0];
            gameObj.transform.localScale = new Vector3(0.001f, 0.001f, 0.001f);

            iTween.MoveTo(gameObj, iTween.Hash("path", waypoints, "speed", 30, "oncomplete", "OnMoveComplete"));
            iTween.ScaleTo(gameObj, iTween.Hash("scale", new Vector3(0.004f, 0.005f, 0.004f), "time", 0.3f));
            yield return new WaitForSeconds(0.1f);
            CurrencyController.CreditBalance(ConfigController.instance.GAME_CONFIG.coinConfig.extra_word_coin);
        }
    }



    private void UpdateUI()
    {
        claimButton.SetActive(extraProgress.current >= extraProgress.target);
        if (claimButton.activeInHierarchy)
            DOTweenManager.ButtonActiveEffect(claimButton.transform);
        else
            DOTweenManager.DisableTween(claimButton.transform);

        progressText.text = extraProgress.current + "/" + extraProgress.target;
    }
}

﻿// using GoogleMobileAds.Api;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardedButton : MonoBehaviour
{
    public GameObject content;
    public GameObject adAvailableTextHolder;
    public TimerText timerText;

    private const string ACTION_NAME = "rewarded_video";
    private bool isEventAttached;

//     private void Start()
//     {
//         if (timerText != null) timerText.onCountDownComplete += OnCountDownComplete;

// #if UNITY_ANDROID || UNITY_IOS
//         if (AdmobController.instance.rewardBasedVideo != null)
//         {
//             AdmobController.instance.rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
//         }
//         if (!IsAvailableToShow())
//         {
//             content.SetActive(false);
//             if (IsAdAvailable() && !IsActionAvailable())
//             {
//                 int remainTime = (int)(ConfigController.instance.GAME_CONFIG.admob.rewardedVideoPeriod - CUtils.GetActionDeltaTime(ACTION_NAME));
//                 ShowTimerText(remainTime);
//             }
//         }
// #else
//         content.SetActive(false);
// #endif
//     }

//     public void OnClick()
//     {
//         AdmobController.instance.ShowRewardBasedVideo();
//         Sound.instance.PlayButton();

//         GoogleAnalyticsV3.instance.LogEvent("Rewarded Video", "On Click", "On Click", 0);
//     }

//     private void ShowTimerText(int time)
//     {
//         if (adAvailableTextHolder != null)
//         {
//             adAvailableTextHolder.SetActive(true);
//             timerText.SetTime(time);
//             timerText.Run();
//         }
//     }

//     public void HandleRewardBasedVideoRewarded(object sender, Reward args)
//     {
//         content.SetActive(false);
//         ShowTimerText(ConfigController.instance.GAME_CONFIG.admob.rewardedVideoPeriod);
//     }

//     private void OnCountDownComplete()
//     {
//         adAvailableTextHolder.SetActive(false);
//         if (IsAdAvailable())
//         {
//             content.SetActive(true);
//         }
//     }

//     public bool IsAvailableToShow()
//     {
//         return IsActionAvailable() && IsAdAvailable();
//     }

//     private bool IsActionAvailable()
//     {
//         return CUtils.IsActionAvailable(ACTION_NAME, ConfigController.instance.GAME_CONFIG.admob.rewardedVideoPeriod);
//     }

//     private bool IsAdAvailable()
//     {
//         if (AdmobController.instance.rewardBasedVideo == null) return false;
//         bool isLoaded = AdmobController.instance.rewardBasedVideo.IsLoaded();
//         if (!isLoaded)
//         {
//             AdmobController.instance.RequestRewardBasedVideo();
//         }
//         return isLoaded;
//     }

//     private void OnDestroy()
//     {
// #if UNITY_ANDROID || UNITY_IOS
//         if (AdmobController.instance.rewardBasedVideo != null)
//         {
//             AdmobController.instance.rewardBasedVideo.OnAdRewarded -= HandleRewardBasedVideoRewarded;
//         }
// #endif
//     }

//     private void OnApplicationPause(bool pause)
//     {
//         if (!pause)
//         {
//             if (adAvailableTextHolder.activeSelf)
//             {
//                 int remainTime = (int)(ConfigController.instance.GAME_CONFIG.admob.rewardedVideoPeriod - CUtils.GetActionDeltaTime(ACTION_NAME));
//                 ShowTimerText(remainTime);
//             }
//         }
//     }
}

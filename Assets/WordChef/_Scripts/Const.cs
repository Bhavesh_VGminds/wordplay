﻿using UnityEngine;
using System.Collections;

public class Const
{
    public const float CELL_GAP_COEF = 0.08f;
    public const float COL_GAP_COEF = 0.4f;
    public static int HINT_COST = ConfigController.instance.GAME_CONFIG.coinConfig.hint_cost_coin;
    public static int NUM_SUBWORLD = 2;
    public static bool isLevelFinish=false;
    public static bool doScrumble=false;

    public static int oldPoint=0;
}

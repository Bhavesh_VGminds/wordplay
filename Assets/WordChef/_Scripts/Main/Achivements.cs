﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class Achivements : Dialog
{
    public static Achivements instance;
    public Transform _Container;
    public Transform _MissionItemPrefab, CoinPrefb;

    MissionPrefb missionPrefb;

    public Color[] color;


    protected override void Awake()
    {
        base.Awake();
        instance = this;
    }
    protected override void Start()
    {
        base.Start();
        LoadMissionData();
    }

    void LoadMissionData()
    {
        Prefs.IsAchivementActive = false;
        MissionJsonClass obj = new MissionJsonClass();
        Mission[] mission = obj.GetMissionValue();

        for (int i = 0; i < mission.Length; i++)
        {
            Transform Mission_Item = Instantiate(_MissionItemPrefab);
            Mission_Item.transform.SetParent(_Container.GetComponent<RectTransform>());
            Mission_Item.transform.localScale = Vector3.one;
            missionPrefb = Mission_Item.GetComponent<MissionPrefb>();
            missionPrefb.lblName = mission[i].Name.Replace('"', ' ').Trim();
            missionPrefb.initialValue = mission[i].InitialValue;
            missionPrefb.incrementalValue = mission[i].incrementalValue;
            missionPrefb.Coin = mission[i].Coins;
            missionPrefb.MissionMsg= mission[i].Mission_Message;
            missionPrefb.iconName = mission[i].Icon_Name;
            missionPrefb.icon.color = color[i];
            missionPrefb.LoadMissionData();
        }
        _Container.transform.parent.GetComponent<ScrollRect>().DOVerticalNormalizedPos(1, 1).SetDelay(0.2f);
    }
}

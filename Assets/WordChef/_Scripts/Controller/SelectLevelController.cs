﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class SelectLevelController : BaseController
{
    public RectTransform scrollContent;
    public ScrollRect scrollRect;
    public Sprite forOtherLevelBG;

    public Text txtCatName, txtLevelName;

    protected override void Start()
    {
        base.Start();
        Prefs.currentWorldName = JsonToClass.GetCategories()[Prefs.currentWorld].cn;
        txtCatName.text = Prefs.currentWorldName;
        txtLevelName.text = Prefs.currentSubWorldName;

        scrollRect.DOVerticalNormalizedPos((Prefs.currentLevel > 20) ? 0 : 1, 1).SetDelay(0.2f);

        int numLevels = Superpow.Utils.GetNumLevels(Prefs.currentWorld, Prefs.currentSubWorld);
        scrollContent.GetComponent<Image>().sprite = forOtherLevelBG as Sprite;

        for (int i = 0; i < numLevels; i++)
        {
            GameObject levelButton = Instantiate(MonoUtils.instance.levelButton);
            levelButton.transform.SetParent(scrollContent);
            levelButton.transform.localScale = Vector3.one;
        }
    }
}

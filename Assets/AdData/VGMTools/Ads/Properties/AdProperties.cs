﻿namespace VGMTools
{
	namespace Ads
	{
		namespace Properties
		{
			public interface IRequestAd
			{
				void RequestAd ();
			}

			public interface IShowAd<J>
			{
				void ShowAd (J j);
			}

			public interface IHideAd
			{
				void HideAd ();
			}

			public interface IAdComplete
			{
				void OnAdFinished ();
			}

			public interface IAdReady
			{
				bool IsAdReady ();
			}

			//TestCode
			public enum BannerPos
			{
				Top =1,
				Bottom = 0
			}
		}
	}
}
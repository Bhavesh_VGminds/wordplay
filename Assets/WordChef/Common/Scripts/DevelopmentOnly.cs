﻿using UnityEngine;
using System.Collections;
using System.IO;

public class DevelopmentOnly : MonoBehaviour
{
    public bool setRuby;
    public int ruby;

    public bool unlockWorld;
    public int worldValue;
    public bool unlockSubWorld;
    public int subWorldValue;
    public bool unlockLevel;
    public int levelValue;
    public bool rewardVideo;
    public int videoValue;
    public bool purchaseItem;
    public int itemValue;
    public bool login;
    public int loginValue;

    public bool clearAllPrefs;

    private void Start()
    {
        if (setRuby)
            CurrencyController.SetBalance(ruby);

        if (unlockWorld)
            Prefs.unlockedWorld = worldValue;

        if (unlockSubWorld)
            Prefs.unlockedSubWorld = subWorldValue;

        if (unlockLevel)
            Prefs.unlockedLevel = levelValue;

        if (clearAllPrefs)
        {
            CPlayerPrefs.DeleteAll();
            CPlayerPrefs.Save();
        }

        if (login)
            Prefs.GetTotalDaysOfLogin = loginValue;

        if (rewardVideo)
            Prefs.totalAdWatchAdded = videoValue;

        if(purchaseItem)
           Prefs.totalPurchaseItemAdded=itemValue;
    }
}

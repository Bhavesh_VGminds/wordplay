﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{
    public Text levelText;
    public Color cLocked, cUnLoacked;
    public int world, subWorld, level;
    public Transform centerPoint, PanImage, ImgDone;
    public Image background;

    private List<Vector3> letterLocalPositions = new List<Vector3>();
    private List<Text> letterTexts = new List<Text>();
    private GameLevel gameLevel;
    private const float RADIUS = 40f;

    public void Start()
    {
        world = Prefs.currentWorld;
        subWorld = Prefs.currentSubWorld;
        level = transform.GetSiblingIndex();
        levelText.enabled = true;
        levelText.text = (level + 1).ToString();
        GetComponent<Button>().onClick.AddListener(OnButtonClick);

        // gameLevel = Resources.Load<GameLevel>((("World_" + world + "/SubWorld_" + subWorld + "/Level_" + level);
        gameLevel = JsonToClass.getWorldLevel(world, subWorld, level);


        int unlockedWorld = Prefs.unlockedWorld;
        int unlockedSubWorld = Prefs.unlockedSubWorld;
        int unlockedLevel = Prefs.unlockedLevel;

        if (world < unlockedWorld ||
            (world == unlockedWorld && subWorld < unlockedSubWorld) ||
            (world == unlockedWorld && subWorld <= unlockedSubWorld && level < unlockedLevel))
        {
            ImgDone.gameObject.SetActive(true);
            background.GetComponentInChildren<Image>().color = cUnLoacked;
        }
        else if (world == unlockedWorld && subWorld == unlockedSubWorld && level == unlockedLevel)
        {
            this.GetComponent<Animator>().enabled = false;
            DOTweenManager.ButtonActiveEffect(this.transform);
            ImgDone.gameObject.SetActive(false);
            if (gameLevel != null)
                Load();
            background.GetComponentInChildren<Image>().color = cUnLoacked;

        }
        else
        {
            ImgDone.gameObject.SetActive(false);
            centerPoint.gameObject.SetActive(false);
            GetComponent<Button>().interactable = false;
            background.GetComponentInChildren<Image>().color = cLocked;

        }
    }

    public void Load()
    {
        string alphabets = gameLevel.word.Replace("\"", string.Empty).Trim();
        Debug.Log("Word:" + alphabets);

        int numLetters = alphabets.Length;
        if (numLetters == 0) return;

        float delta = 360f / numLetters;

        float angle = 90;
        for (int i = 0; i < numLetters; i++)
        {
            float angleRadian = angle * Mathf.PI / 180f;
            float x = Mathf.Cos(angleRadian);
            float y = Mathf.Sin(angleRadian);
            Vector3 position = RADIUS * new Vector3(x, y, 0);

            letterLocalPositions.Add(position);

            angle += delta;
        }

        for (int i = 0; i < numLetters; i++)
        {

            Text letter = Instantiate(MonoUtils.instance.letter);
            letter.transform.SetParent(centerPoint);
            letter.transform.localScale = Vector3.one;
            letter.text = alphabets[i].ToString().ToUpper();
            letter.fontSize = 35;
            letterTexts.Add(letter);
        }

        List<int> indexes = Prefs.GetPanWordIndexes(world, subWorld, level).ToList();
        if (indexes.Count != numLetters)
        {
            indexes = Enumerable.Range(0, numLetters).ToList();
            indexes.Shuffle(level);
            Prefs.SetPanWordIndexes(world, subWorld, level, indexes.ToArray());
        }

        for (int i = 0; i < numLetters; i++)
        {
            letterTexts[i].transform.localPosition = letterLocalPositions[indexes.IndexOf(i)];
        }
    }

    public void OnButtonClick()
    {
        Prefs.currentWorld = world;
        Prefs.currentSubWorld = subWorld;
        Prefs.currentLevel = level;

        CUtils.LoadScene(0, true);
        Sound.instance.PlayButton();
    }
}

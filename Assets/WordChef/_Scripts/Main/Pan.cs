﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using System.Text;
using DG.Tweening;

public class Pan : MonoBehaviour
{
    private int numLetters;
    private string word, panWord;
    private GameLevel gameLevel;
    private const float RADIUS = 90;
    private List<Vector3> letterPositions = new List<Vector3>();
    private List<Vector3> letterLocalPositions = new List<Vector3>();
    private List<Text> letterTexts = new List<Text>();
    private List<int> indexes = new List<int>();

    private int world, subWorld, level;

    public Transform centerPoint;
    public TextPreview textPreview;

    public static Pan instance;

    public Image hand;
    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        world = Prefs.currentWorld;
        subWorld = Prefs.currentSubWorld;
        level = Prefs.currentLevel;
    }
    string alphabets = "";
    public void Load(GameLevel gameLevel)
    {
        this.gameLevel = gameLevel;
        alphabets = gameLevel.word.Replace("\"", string.Empty).Trim();
        numLetters = alphabets.Length;

        if (numLetters <= 3)
            LineDrawer.instance.RADIUS = 0.65f;
        else if (numLetters <= 5)
            LineDrawer.instance.RADIUS = 0.45f;
        else
            LineDrawer.instance.RADIUS = 0.40f;

        float delta = 360f / numLetters;

        float angle = 90;
        for (int i = 0; i < numLetters; i++)
        {
            float angleRadian = angle * Mathf.PI / 180f;
            float x = Mathf.Cos(angleRadian);
            float y = Mathf.Sin(angleRadian);
            Vector3 position = RADIUS * new Vector3(x, y, 0);

            letterLocalPositions.Add(position);
            letterPositions.Add(centerPoint.TransformPoint(position));

            angle += delta;
        }

        LineDrawer.instance.letterPositions = letterPositions;

        for (int i = 0; i < numLetters; i++)
        {
            Text letter = Instantiate(MonoUtils.instance.letter);
            letter.transform.SetParent(centerPoint);
            letter.transform.localScale = Vector3.one;
            // letter.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(-10, 10)));
            letter.text = alphabets[i].ToString().ToUpper();

            if (alphabets[i].ToString().ToLower() == gameLevel.specialChar.ToLower())
            {
                letter.color = Color.green;
                if (!Prefs.IsSpecialCharOverlay)
                {
                    MainController.instance.isOverlayActive = true;
                    MainController.instance._overlay_SpecialChar.gameObject.SetActive(true);
                    Prefs.IsSpecialCharOverlay = true;
                }

            }

            letter.fontSize = 50;
            letterTexts.Add(letter);
        }

        indexes = Prefs.GetPanWordIndexes(world, subWorld, level).ToList();
        if (indexes.Count != numLetters)
        {
            indexes = Enumerable.Range(0, numLetters).ToList();
            indexes.Shuffle(level);
            Prefs.SetPanWordIndexes(world, subWorld, level, indexes.ToArray());
        }

        GetPanWord();

        for (int i = 0; i < numLetters; i++)
        {
            letterTexts[i].transform.localPosition = letterLocalPositions[indexes.IndexOf(i)];
        }

        for (int i = 0; i < letterTexts.Count; i++)
            letterTexts[i].transform.DOMove(centerPoint.transform.position, 1).From();

        if (level == 0 && subWorld == 0 && world == 0)
        {
            hand.gameObject.SetActive(true);
            hand.DOFade(0, 1).SetDelay(1).From().OnComplete(() => TweenCallBack());
        }
    }

    void TweenCallBack()
    {
        hand.transform.position = letterTexts[0].transform.position;
        hand.transform.DOMove(letterTexts[1].transform.position, 1).SetLoops(-1, LoopType.Restart);
    }

    private void GetShuffeWord()
    {
        List<int> origin = new List<int>();
        origin.AddRange(indexes);
        while (true)
        {
            indexes.Shuffle();
            if (!origin.SequenceEqual(indexes)) break;
        }
        GetPanWord();
    }

    private void GetPanWord()
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < numLetters; i++)
        {
            sb.Append(alphabets[indexes[i]]);
        }
        panWord = sb.ToString();
        textPreview.word = panWord.ToUpper();
    }

    public void Shuffle()
    {

        GetShuffeWord();
        Prefs.SetPanWordIndexes(world, subWorld, level, indexes.ToArray());

        int i = 0;
        foreach (var text in letterTexts)
        {
            iTween.MoveTo(text.gameObject, iTween.Hash("position", letterLocalPositions[indexes.IndexOf(i)], "time", 0.15f, "isLocal", true));
            /// text.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(-10, 10)));
            i++;
        }
        Sound.instance.Play(Sound.Others.Suffle);
    }
}

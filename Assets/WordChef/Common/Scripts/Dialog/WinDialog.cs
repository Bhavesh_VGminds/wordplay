﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class WinDialog : Dialog
{
    public Text points;
    public GameObject coin, particlePrefb;
    private int numLevels;
    private bool isLastLevel;
    private int subWorld, level;
    GameObject particle = null;

    protected override void Start()
    {
        base.Start();
        CheckUnlock();
        GetComponent<Canvas>().sortingLayerName = "UI2";
    }
    private void CheckUnlock()
    {

        //  points.DOText(Prefs.totalWordFindAdded.ToString(), 1f, true, ScrambleMode.Numerals, Const.oldPoint.ToString());

        numLevels = Superpow.Utils.GetNumLevels(Prefs.currentWorld, Prefs.currentSubWorld);
        subWorld = Prefs.currentSubWorld;
        level = Prefs.currentLevel;

        Const.NUM_SUBWORLD = JsonToClass.GetTotalSection(Prefs.currentWorld);

        isLastLevel = Prefs.IsLastLevel();

        Prefs.currentLevel = (level + 1) % numLevels;
        if (level == numLevels - 1)
        {
            Prefs.currentSubWorld = (subWorld + 1) % Const.NUM_SUBWORLD;
            title.GetComponent<Text>().text = "Level Up !";
            AnalyticsScript.Instance.SendEvent("Level" + Prefs.currentLevel.ToString(), "AnalyticsScript", "INitialized_analytics " + "WinDialog");

            if (subWorld == Const.NUM_SUBWORLD - 1)
            {
                Debug.Log(subWorld.ToString() + "Is Last Level :" + (Const.NUM_SUBWORLD - 1).ToString());
                Prefs.LastWorld = Prefs.unlockedWorld;
                Prefs.currentWorld++;

                Debug.Log("@@@  Prefs.currentWorld :" + Prefs.currentWorld.ToString());
                Prefs.unlockedWorld = Prefs.currentWorld;
                Debug.Log("@@@  Prefs.unlockedWorld :" + Prefs.unlockedWorld.ToString());
                title.GetComponent<Text>().text = "World Up !";
            }
        }
        else
        {
            title.GetComponent<Text>().text = "Word Up !";
        }

        if (isLastLevel)
        {
            Prefs.unlockedWorld = Prefs.currentWorld;
            Prefs.unlockedSubWorld = Prefs.currentSubWorld;
            Prefs.unlockedLevel = Prefs.currentLevel;

            // Debug.Log("@@@**  Prefs.currentWorld :" + Prefs.currentWorld.ToString());
            Prefs.unlockedWorld = Prefs.currentWorld;
            // Debug.Log("@@@1=**  Prefs.unlockedWorld :" + Prefs.unlockedWorld.ToString());
        }

        StartCoroutine(NextClick());
    }

    IEnumerator NextClick()
    {
        yield return new WaitForSeconds(0.5f);
        Sound.instance.PlayButton();
        if (particle == null)
        {
            ConfigController.instance.CreateFlyingCoin(coin.transform.position, isLastLevel ? ConfigController.instance.Config().coinConfig.word_cleared : 0);
            particle = Instantiate(particlePrefb);
            Invoke("CloseButton", 1.5f);
        }

    }

    void CloseButton()
    {
        Destroy(particle);

        Close();

        if (((Prefs.LastWorld == Prefs.unlockedWorld - 1) || Prefs.unlockedWorld == 777) && Prefs.LastWorld >= 0)
        {
            DialogController.instance.ShowDialog(DialogType.Medals);
        }
        else if (level == numLevels - 1)
        {
            DialogController.instance.ShowDialog(DialogType.LevelUp);
        }
        else
        {
            CUtils.LoadScene(0, true);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class DailyBonus : Dialog
{

    public GameObject[] activeBonus;
    public GameObject[] notActiveBonus;
    public Text[] bonus;
    public Button btnClaim;
     GameObject todayObj;
    int CurrentDayAmount=0;




    // Use this for initialization
   protected override void Start()
    {
        base.Start();

        bonus[0].text = ConfigController.instance.GAME_CONFIG.coinConfig.dailyCoinsConfig.daily_login_coins_1.ToString();
        bonus[1].text = ConfigController.instance.GAME_CONFIG.coinConfig.dailyCoinsConfig.daily_login_coins_2.ToString();
        bonus[2].text = ConfigController.instance.GAME_CONFIG.coinConfig.dailyCoinsConfig.daily_login_coins_3.ToString();
        bonus[3].text = ConfigController.instance.GAME_CONFIG.coinConfig.dailyCoinsConfig.daily_login_coins_4.ToString();
        bonus[4].text = ConfigController.instance.GAME_CONFIG.coinConfig.dailyCoinsConfig.daily_login_coins_5.ToString();
        bonus[5].text = ConfigController.instance.GAME_CONFIG.coinConfig.dailyCoinsConfig.daily_login_coins_6.ToString();
        bonus[6].text = ConfigController.instance.GAME_CONFIG.coinConfig.dailyCoinsConfig.daily_login_coins_7.ToString();


        for (int i = 1; i <= activeBonus.Length; i++)
        {
            
            if (i <= Prefs.LastdayNumber && Prefs.LastdayNumber == Prefs.GetTotalDaysOfLogin)
            {
                activeBonus[i - 1].SetActive(true);
                btnClaim.GetComponentInChildren<Text>().text="Ok";
            }
            else if (i == Prefs.GetTotalDaysOfLogin && Prefs.LastdayNumber != Prefs.GetTotalDaysOfLogin)
            {
                todayObj=activeBonus[i - 1];
                DOTweenManager.ButtonActiveEffect(notActiveBonus[i-1].transform);
                CurrentDayAmount=int.Parse(bonus[i-1].text);
                btnClaim.GetComponentInChildren<Text>().text="Claim";
            }
            else
            {
                activeBonus[i - 1].gameObject.SetActive(false);
            }
        }
       
    }

   public void OnValueChange()
    {
        if (CurrentDayAmount != 0)
        {
            ConfigController.instance.CreateFlyingCoin(todayObj.transform.position,CurrentDayAmount);
          //  AnalyticsScript.Instance.SendEvent("Daily Reward "+CurrentDayAmount.ToString(), "AnalyticsScript", "INitialized_analytics " + "DailyBonus");
            Prefs.LastdayNumber = Prefs.GetTotalDaysOfLogin;
            todayObj.SetActive(true);
            btnClaim.interactable=false;
          
            Invoke("CloseButton",1);

        }
        else{
              Close();
        }
    }

    

    void CloseButton(){Close();}
}

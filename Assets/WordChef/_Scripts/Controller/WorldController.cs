﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using DG.Tweening;

public class WorldController : BaseController
{

    public static WorldController instence;
    public Transform _categoryContainer, _levelsParent;
    public Worlds _categoryPrefabs;
    public WorldItem _sectionPrefabs;
    public GameObject _particleGroup;
    public ToggleGroup toggleGroup;
    public ScrollRect scrollRect;
    public List<LayoutElement> levelsList;
    public List<Worlds> WorldList;

    public int totalWorld = 0;
    public int totalLevel = 0;

    public Color[] color;
    string jSonFileName;

    protected override void Awake()
    {
        instence = this;
    }


    protected override void Start()
    {
        base.Start();
        levelsList = new List<LayoutElement>();
        WorldList = new List<Worlds>();

        CreateWorld();
    }

    void CreateWorld()
    {
        Category[] worldJson = JsonToClass.GetCategories();
        totalWorld = worldJson.Length;

        Sprite[] spriteList = Resources.LoadAll<Sprite>("Medals");

        for (int worldIndex = 0; worldIndex < totalWorld; worldIndex++)
        {

            Worlds world = Instantiate(_categoryPrefabs) as Worlds;
            world.transform.SetParent(_categoryContainer.transform);
            world.Index = worldIndex;
            WorldList.Add(world);
            world.WorldName.text = worldJson[worldIndex].cn;
            world.Badge.sprite = spriteList.Single(s => s.name == worldJson[worldIndex].cn);
            world.BadgeBG.color = new Color(color[worldIndex].r, color[worldIndex].g, color[worldIndex].b, color[worldIndex].a);

            world.TotalLevel = JsonToClass.GetSection(worldIndex).Length.ToString();
            world.LevelContainer();
        }


        if ((Prefs.LastWorld == Prefs.unlockedWorld - 1) && Prefs.LastWorld >= 0)
        {
            StartCoroutine(DisplayCurrentWord(WorldList[Prefs.LastWorld]));
        }
        else if (Prefs.unlockedWorld == 777)
        {
            StartCoroutine(DisplayCurrentWord(WorldList[Prefs.currentWorld]));
        }
        else
        {
            WorldList[Prefs.currentWorld].WorldToggle.isOn = true;
        }

    }

    int Temp_LevelCounter = 0;
    public void CreateLevel(int categoryNo, Transform levelParent)
    {
        Levels[] section = JsonToClass.GetSection(categoryNo);
        totalLevel = section.Length;

        float levelPanelHeight = 0.0f;
        if (section.Length > 0)
        {
            for (int secNo = 0; secNo < section.Length; secNo++)
            {
                WorldItem sec = Instantiate(_sectionPrefabs) as WorldItem;
                sec.GetComponent<RectTransform>().SetParent(levelParent);
                levelPanelHeight += sec.GetComponent<LayoutElement>().preferredHeight
                                 + levelParent.GetComponent<VerticalLayoutGroup>().spacing
                                 + levelParent.GetComponent<VerticalLayoutGroup>().padding.top
                                 + levelParent.GetComponent<VerticalLayoutGroup>().padding.bottom;

                // sec..text = "Level " + (Temp_LevelCounter++.ToString());
                sec.name = section[secNo].ln.ToString();
                sec.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
                sec.GetComponent<RectTransform>().localPosition = Vector3.Normalize(Vector3.one);
                if (secNo == section.Length - 1)
                    sec.transform.Find("Line").GetComponent<Image>().enabled = false;
            }

            levelParent.GetComponent<LayoutElement>().preferredHeight = levelPanelHeight;
            levelParent.GetComponent<RectTransform>().sizeDelta = new Vector2(levelParent.GetComponent<LayoutElement>().preferredWidth, levelPanelHeight);

        }
        else
        {
            Debug.Log("Sections are not Available");
        }
    }

    public IEnumerator DisplayCurrentWord(Worlds worldObj)
    {
        Debug.Log("Unlocked World No:" + Prefs.unlockedWorld.ToString());

        Transform DoneImg = worldObj.isDone;
        int LastCatIndex = (Prefs.unlockedWorld == 777) ? Prefs.currentWorld : Prefs.unlockedWorld - 1;

        Worlds LastWorld = WorldList[LastCatIndex];
        ParticleSystem MedaleParticle = worldObj.GetComponentInChildren<ParticleSystem>();

        LastWorld.OutLine.enabled = true;
        LastWorld.OutLine.effectColor = WorldList[LastCatIndex].BadgeBG.color;
        LastWorld.OutLine.DOFade(1, 1).SetLoops(-1, LoopType.Yoyo).From();

        LastWorld.Badge.sortingLayerName = "UI2";
        LastWorld.Badge.sortingOrder = 3;
        LastWorld.Badge.transform.DOScale(new Vector3(600, 600, 600), 1).From();
        LastWorld.Badge.transform.DOLocalMove(new Vector3(280, -500, 0), 1).From().OnComplete(() => MedalMovingTweenComplete(LastWorld, MedaleParticle)); ;

        yield return new WaitForSeconds(1);
        Sound.instance.Play(Sound.Others.Badge);
        GameObject particleGroup = Instantiate(_particleGroup);
        LastWorld.isDone.gameObject.SetActive(true);
        LastWorld.isDone.gameObject.GetComponentsInChildren<Image>()[1].transform.DOScale(1.5f, 2).From();
        LastWorld.Badge.transform.parent.DOShakePosition(1, 5).SetLoops(2, LoopType.Yoyo);

        LastWorld.OutLine.enabled = false;
        LastWorld.OutLine.DOKill();

        if (Prefs.unlockedWorld < totalWorld)
        {
            Worlds nextWorld = WorldList[Prefs.unlockedWorld];
            nextWorld.OutLine.enabled = true;
            nextWorld.OutLine.effectColor = WorldList[Prefs.unlockedWorld].BadgeBG.color;
            nextWorld.OutLine.DOFade(1, 1).SetLoops(-1, LoopType.Yoyo).From();
            if (Prefs.unlockedWorld == 777)
            {
                nextWorld.WorldToggle.isOn = false;
            }
            else
            {
                nextWorld.WorldToggle.isOn = true;
            }

            yield return new WaitForSeconds(2);
            Destroy(particleGroup);

            Prefs.LastWorld = Prefs.unlockedWorld;
        }
    }

    void MedalMovingTweenComplete(Worlds medales, ParticleSystem PS)
    {
        PS.Play();
        medales.Badge.sortingLayerName = "UI";
        medales.Badge.sortingOrder = 0;
    }
}


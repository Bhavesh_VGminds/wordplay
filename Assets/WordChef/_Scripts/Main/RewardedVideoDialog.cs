﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardedVideoDialog : Dialog
{
    public Text amountText;
    public Text messageText;

    public GameObject coinPrefb;

    protected override void Start()
    {
        base.Start();
        SetAmount(ConfigController.instance.Config().admob.rewardedVideoAmount);
    }

    public void SetAmount(int amount)
    {
        amountText.text = amount.ToString();
        messageText.text = string.Format(messageText.text, amount);
    }

    public void Claim()
    {
        Close();
        ConfigController.instance.CreateFlyingCoin(coinPrefb.transform.position, int.Parse(amountText.text));
        Sound.instance.PlayButton();
    }

    public override void Close()
    {
        base.Close();
        Prefs.totalAdWatchAdded++;
    }
}

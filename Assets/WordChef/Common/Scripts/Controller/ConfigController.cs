﻿using UnityEngine;
using System.Collections;
using System.IO;
public class ConfigController : MonoBehaviour
{
    public Main_Config config = new Main_Config();
    public RootRandom rootRandom = null;

    public GameObject coinPrefab;

    public GameConfig GAME_CONFIG = null;
    public GameConfig Config()
    {
        // Debug.Log(GAME_CONFIG.versionConfig.ANDROID == "");
        if (GAME_CONFIG.versionConfig.ANDROID == "")
        {
            GAME_CONFIG = new GameConfig();
            RootConfig rootObject = config.Main_Config_Load();
            GAME_CONFIG = rootObject.gameConfig;

            return GAME_CONFIG;
        }
        else
        {
            Debug.Log("FB :" + GAME_CONFIG.facebookPageID);
            return GAME_CONFIG;
        }
    }


    public static ConfigController instance;

    private void Awake()
    {
        Config();
        instance = this;
        WordHitsDigits wordHitsDigits = new WordHitsDigits();
        rootRandom = new RootRandom();
        rootRandom = wordHitsDigits.rootRandom;

    }

    void Start()
    {
    }

    public void CreateFlyingCoin(Vector3 startPos, int coinAmt)
    {
        GameObject Coin = Instantiate(coinPrefab);
        Coin.transform.position = startPos;
        Coin.GetComponent<CoinFly>().NoOFCoin = coinAmt;
        Coin.transform.localScale = new Vector3(0.005f, 0.005f, 0.005f);
    }

    public string GetWordHits(int wordIndex)
    {
        return rootRandom.randomNo[wordIndex].Replace("[", string.Empty).Replace("\"", string.Empty).Replace("]", string.Empty);
    }
}

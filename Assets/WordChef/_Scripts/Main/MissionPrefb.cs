﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MissionPrefb : MonoBehaviour
{

    public Text lblMissionMsg, lblTarget, lblAmount;
    public Color btnOnColor, btnOffColor;
    public string MissionMsg;
    public string lblName;
    public Button btnCoin, coinStack;
    public Image icon, progressBar, line;
    public int initialValue, incrementalValue;
    public string iconName;
    public int Coin = 0;
    private int NetCoin = 0, diffCoin = 0;
    float FillValue = 0.0f;
    string targetMsg;

    int multi = 0, StoredValue = 0;


    void Start()
    {
        // btnCoin.onClick.AddListener(() => BtnOnClick());
    }
    public void LoadMissionData()
    {
        switch (lblName)
        {
            case "Login":
                if (!PlayerPrefs.HasKey("MLogin")) Prefs.MLogin = initialValue;
                initialValue = Prefs.MLogin;
                StoredValue = Prefs.GetTotalDaysOfLogin;
                break;
            case "Levels":
                if (!PlayerPrefs.HasKey("MLevels")) Prefs.MLevels = initialValue;
                initialValue = Prefs.MLevels;
                StoredValue = Prefs.TotalUplockedLevelAdded;

                break;
            case "Words":
                if (!PlayerPrefs.HasKey("MWords")) Prefs.MWords = initialValue;
                initialValue = Prefs.MWords;
                StoredValue = Prefs.totalWordFindAdded;

                break;
            case "Hidden_Words":
                if (!PlayerPrefs.HasKey("MHidden_Words")) Prefs.MHidden_Words = initialValue;
                initialValue = Prefs.MHidden_Words;
                StoredValue = Prefs.totalExtraAdded;

                break;
            case "Hints":
                if (!PlayerPrefs.HasKey("MHints")) Prefs.MHints = initialValue;
                initialValue = Prefs.MHints;
                StoredValue = Prefs.totalHintAdded;

                break;
            case "Reward_Video":
                if (!PlayerPrefs.HasKey("MReward_Video")) Prefs.MReward_Video = initialValue;
                initialValue = Prefs.MReward_Video;
                StoredValue = Prefs.totalAdWatchAdded;

                break;
            case "Purchase_Item":
                if (!PlayerPrefs.HasKey("MPurchase_Item")) Prefs.MPurchase_Item = initialValue;
                initialValue = Prefs.MPurchase_Item;
                StoredValue = Prefs.totalPurchaseItemAdded;

                line.enabled = false;
                break;
        }
        FillValue = float.Parse(StoredValue.ToString()) / float.Parse(initialValue.ToString());
        int temp = (StoredValue == 0) ? incrementalValue : StoredValue;
        diffCoin = (temp < initialValue) ? initialValue - temp : temp - initialValue;

        lblTarget.text = StoredValue.ToString() + "/" + initialValue.ToString();
        lblMissionMsg.text = string.Format(MissionMsg, initialValue.ToString());
        
        multi =((StoredValue < initialValue ) ? 0 : 1) + int.Parse(Mathf.CeilToInt(diffCoin / incrementalValue).ToString());
        NetCoin = (multi > 1) ? (multi * Coin) : Coin;
        lblAmount.text = NetCoin.ToString();
        Progress();
    }

    public void BtnOnClick()
    {
        Debug.Log(multi.ToString());
        switch (lblName)
        {
            case "Login":

                initialValue = (initialValue + Prefs.GetTotalDaysOfLogin) - (Prefs.GetTotalDaysOfLogin - (multi * initialValue));
                Prefs.MLogin = initialValue;
                break;
            case "Levels":
                initialValue = (initialValue + Prefs.TotalUplockedLevelAdded) - (Prefs.TotalUplockedLevelAdded - (multi * initialValue));
                Prefs.MLevels = initialValue;
                break;
            case "Words":
                initialValue = (initialValue + Prefs.totalWordFindAdded) - (Prefs.totalWordFindAdded - (multi * initialValue));
                Prefs.MWords = initialValue;
                break;
            case "Hidden_Words":
                initialValue = (initialValue + Prefs.totalExtraAdded) - (Prefs.totalExtraAdded - (multi * initialValue));
                Prefs.MHidden_Words = initialValue;
                break;
            case "Hints":
                initialValue = (initialValue + Prefs.totalHintAdded) - (Prefs.totalHintAdded - (multi * initialValue));
                Prefs.MHints = initialValue;

                break;
            case "Reward_Video":
                initialValue = (initialValue + Prefs.totalAdWatchAdded) - (Prefs.totalAdWatchAdded - (multi * initialValue));
                Prefs.MReward_Video = initialValue;

                break;
            case "Purchase_Item":
                initialValue = (initialValue + Prefs.totalPurchaseItemAdded) - (Prefs.totalPurchaseItemAdded - (multi * initialValue));
                Prefs.MPurchase_Item = initialValue;
                break;
        }
        AnalyticsScript.Instance.SendEvent(lblName, "AnalyticsScript", "INitialized_analytics " + "MissionPrefb");
        LoadMissionData();
        ConfigController.instance.CreateFlyingCoin(Achivements.instance.CoinPrefb.transform.position, int.Parse(lblAmount.text));
    }

    void Progress()
    {
        if (FillValue >= 0.99f)
        {
            btnCoin.GetComponent<Image>().color = btnOnColor;
            coinStack.interactable = false;
            coinStack.GetComponent<Image>().raycastTarget = false;
            btnCoin.interactable = true;
            lblAmount.enabled = true;
            Prefs.IsAchivementActive = true;
        }
        else
        {
            btnCoin.GetComponent<Image>().color = btnOffColor;
            btnCoin.interactable = false;
            coinStack.GetComponent<Image>().raycastTarget = true;
            coinStack.interactable = true;
            lblAmount.enabled = true;
        }
        progressBar.fillAmount = FillValue;
    }
}

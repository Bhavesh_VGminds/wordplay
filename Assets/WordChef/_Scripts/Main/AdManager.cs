﻿using UnityEngine;
using VGMTools.Ads.Properties;
using VGMTools.Ads.Relations;

namespace VGMTools
{
  namespace Ads
  {
    namespace Manager
    {
      public class AdManager : MonoBehaviour
      {
        private static IGenericAdTag[] AdsCollection;

        void Awake ()
        {
          AdsCollection = GetComponents<IGenericAdTag> ();
        }

        public static bool IsAdReady (IGenericAdTag ad)
        {
          IAdReady readyableAd = ad as IAdReady;
          if (readyableAd == null || readyableAd.IsAdReady ())
          {
            return true;
          }
          return false;
        }

        public static bool IsAdReady<T> () where T : IGenericAdTag
        {
          foreach (IGenericAdTag ad in AdsCollection)
          {
            if (ad is T)
            {
              if (IsAdReady (ad))
              {
                return true;
              }
            }
          }
          return false;
        }

        public static void RequestAd ()
        {
          foreach (IGenericAdTag ad in AdsCollection)
          {
            RequestAd (ad);
          }
        }

        public static void RequestAd<T> ()  where T : IGenericAdTag
        {
          foreach (IGenericAdTag ad in AdsCollection)
          {
            if (ad is T && !IsAdReady (ad))
            {
              RequestAd (ad);
            }
          }
        }

        public static void RequestAd (IGenericAdTag ad)
        {
          IRequestAd requestableAd = ad as IRequestAd;
          if (requestableAd != null && !IsAdReady (ad))
            requestableAd.RequestAd ();
        }

        public static void ShowAd<T, J> (J j) where T : IGenericAdTag
        {
          foreach (IGenericAdTag ad in AdsCollection)
          {
            if (ad is T)
            {
              IShowAd<J> showableAd = ad as IShowAd<J>;
              if (showableAd != null && IsAdReady (ad))
              {
                Debug.Log ("adReady");
                showableAd.ShowAd (j);
                return;
              }

              if (showableAd == null)
              {
                Debug.Log ("wrong type");
              }
              else
              {
                Debug.Log ("right type");
                Debug.Log (IsAdReady (ad));
              }
            }
          }
        }

        public static void ShowAd<J> (J j)
        {
          foreach (IGenericAdTag ad in AdsCollection)
          {
            if (ad is IShowAd<J>)
            {
              IShowAd<J> showableAd = ad as IShowAd<J>;
              if (showableAd != null && IsAdReady (ad))
              {
                showableAd.ShowAd (j);
                return;
              }
            }
          }
        }

        public static void HideAd ()
        {
          foreach (IGenericAdTag ad in AdsCollection)
          {
            HideAd (ad);
          }
        }

        public static void HideAd<T> ()
        {
          foreach (IGenericAdTag ad in AdsCollection)
          {
            if (ad is T)
            {
              HideAd (ad);
            }
          }
        }

        public static void HideAd (IGenericAdTag ad)
        {
          IHideAd hideableAd = ad as IHideAd;
          if (hideableAd != null)
            hideableAd.HideAd ();
        }
      }
    }
  }
}
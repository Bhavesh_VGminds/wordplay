﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Cell : MonoBehaviour
{
    public Text letterText;
    public int LineWordNo;
    public string letter;
    public bool isShown,isExist;
    public Color color;
    public ParticleSystem particleSystem;

    public Image coin;

    private Vector3 originLetterScale;

    void Start(){
        isExist=false;
    }

    public void Animate()
    {
        Vector3 beginPosition = TextPreview.instance.transform.position;
        originLetterScale = letterText.transform.localScale;
        Vector3 middlePoint = CUtils.GetMiddlePoint(beginPosition, transform.position, -0.3f);
        Vector3[] waypoint = { beginPosition, middlePoint, transform.position };
        ShowText();
        letterText.transform.position = beginPosition;
        letterText.transform.localScale = TextPreview.instance.text.transform.localScale;
        letterText.transform.SetParent(MonoUtils.instance.textFlyTransform);
        iTween.MoveTo(letterText.gameObject, iTween.Hash("path", waypoint, "time", 0.2f, "oncomplete", "OnMoveToComplete", "oncompletetarget", gameObject));
        iTween.ScaleTo(letterText.gameObject, iTween.Hash("scale", originLetterScale, "time", 0.2f));
    }

    private void OnMoveToComplete()
    {
        letterText.transform.SetParent(transform);
        iTween.ScaleTo(letterText.gameObject, iTween.Hash("scale", originLetterScale * 1.3f, "time", 0.15f, "oncomplete", "OnScaleUpComplete", "oncompletetarget", gameObject));
    }

    private void OnScaleUpComplete()
    {
        iTween.ScaleTo(letterText.gameObject, iTween.Hash("scale", originLetterScale, "time", 0.15f));
        
    }

    public void ShowHint()
    {
        isShown = true;
        originLetterScale = letterText.transform.localScale;
        ShowText();
        OnMoveToComplete();
    }

    public void ShowText()
    {
        isShown = true;
        letterText.text = letter;
        this.gameObject.GetComponent<Image>().color=new Color(color.r,color.g,color.b,1);

        if(!isExist){
            isExist=true;
            float particleSize=this.gameObject.GetComponent<RectTransform>().rect.width * 2;
            if(particleSize <=100)
              particleSystem.gameObject.transform.localScale=new Vector3(particleSize,particleSize,particleSize);
            particleSystem.Play();
        }
        DOTweenManager.CellShaking(this.transform);
        if(coin.enabled){
            coin.enabled=false;
            ConfigController.instance.CreateFlyingCoin(this.transform.position,1);
            CUtils.setLastGameFlag(LineWordNo);
        }
    }
}

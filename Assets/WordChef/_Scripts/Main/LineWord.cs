﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LineWord : MonoBehaviour
{

    public int lineNo;

    public string answer;
    public float cellSize;
    public List<Cell> cells = new List<Cell>();
    public int numLetters;
    public float lineWidth;

    public bool isShown;

    public void SetProgress(string progress)
    {
        isShown = true;
        int i = 0;
        foreach (var cell in cells)
        {
            if (progress[i] == '1')
            {
                cell.isShown = true;
                cell.letterText.text = cell.letter;
            }
            else
            {
                isShown = false;
            }
            i++;
        }
    }

    public void ShowAnswer()
    {
        isShown = true;
        foreach (var cell in cells)
        {
            cell.isShown = true;
            cell.LineWordNo = lineNo;
        }

        StartCoroutine(IEShowAnswer());
    }

    public IEnumerator IEShowAnswer()
    {
        foreach (var cell in cells)
        {
            cell.isShown = true;
            if (!cell.isExist)
                cell.Animate();
            else
                cell.ShowText();
            yield return new WaitForSeconds(0.1f);
        }
    }

    public void ShowHint()
    {
        for (int i = 0; i < cells.Count; i++)
        {
            var cell = cells[i];
            if (!cell.isShown)
            {
                cell.ShowHint();
                if (i == cells.Count - 1)
                {
                    isShown = true;
                }
                return;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class CoinFly : MonoBehaviour
{

    // Use this for initialization

    Vector3 DesinationPoint;
    public int NoOFCoin = 0;
    Transform EndObj = null;

    void Awake()
    {
    }

    void Start()
    {
        EndObj = GameObject.Find("NewHeader").transform.Find("Shop").transform.Find("ImgCoin");
        DesinationPoint = new Vector3(EndObj.position.x + 0.1f, EndObj.position.y, EndObj.position.z);
        this.transform.DOLocalMove(DesinationPoint, 1f);
        this.transform.DOScale(new Vector3(0.003f, 0.003f, 0.003f), 1f).OnComplete(CompleteTween);
    }

    void CompleteTween()
    {
        Destroy(this.gameObject);
        // EndObj.transform.DOShakePosition(1,4);
        EndObj.GetComponentInChildren<ParticleSystem>().Play();
        CurrencyController.CreditBalance(NoOFCoin);
        Sound.instance.Play(Sound.Others.CoinCollection, 0.2f);
    }

    // Update is called once per frame
    void Update()
    {

    }
}

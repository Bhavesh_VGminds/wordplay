﻿namespace VGMTools
{
	namespace Ads
	{
		namespace Relations
		{
			namespace Shared
			{
				public interface IFullScreenTag : IGenericAdTag
				{

				}

				public interface IPartialScreenTag : IGenericAdTag
				{

				}
			}
		}
	}
}
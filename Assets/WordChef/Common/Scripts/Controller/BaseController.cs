﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SimpleJSON;
using VGMTools.Ads.Manager;
using VGMTools.Ads.Properties;
using VGMTools.Ads.Relations.Shared;

public class BaseController : MonoBehaviour
{
    public GameObject donotDestroyOnLoad;
    public string sceneName;
    public Sound.Others sound = Sound.Others.None;
    protected int numofEnterScene;

    public bool isShowBanner = false;

    protected virtual void Awake()
    {
        if (DonotDestroyOnLoad.instance == null && donotDestroyOnLoad != null)
            Instantiate(donotDestroyOnLoad);

        iTween.dimensionMode = CommonConst.ITWEEN_MODE;
        CPlayerPrefs.useRijndael(CommonConst.ENCRYPTION_PREFS);

        numofEnterScene = CUtils.IncreaseNumofEnterScene(sceneName);
    }

    protected virtual void Start()
    {
        CPlayerPrefs.Save();
        if (JobWorker.instance.onEnterScene != null)
        {
            JobWorker.instance.onEnterScene(sceneName);
        }

#if UNITY_WSA && !UNITY_EDITOR
        StartCoroutine(SavePrefs());
#endif

        if (isShowBanner && Prefs.currentWorld > 0)
        {
            CUtils.ShowBannerAd();
        }
        else
        {
            CUtils.CloseBannerAd();
        }

        Invoke("PlaySound", 0.2f);
    }

    void PlaySound() { Sound.instance.Play(sound); }

    public virtual void OnApplicationPause(bool pause)
    {
        // Debug.Log("On Application Pause");
        CPlayerPrefs.Save();
        if (pause == false)
        {
            // Timer.Schedule(this, 0.5f, () =>
            // {
            //     CUtils.ShowInterstitialAd();
            // });
        }
    }

    private IEnumerator SavePrefs()
    {
        while (true)
        {
            yield return new WaitForSeconds(5);
            CPlayerPrefs.Save();
        }
    }

    private const string ACTION_NAME = "rewarded_video";


    private void OnDestroy()
    {

    }
}

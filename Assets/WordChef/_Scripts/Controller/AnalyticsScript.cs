﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Analytics;
using UnityEngine.Analytics;
using System;

public class AnalyticsScript : MonoBehaviour
{
    public static AnalyticsScript Instance;

    void Awake(){
        Instance=this;
    }

    void Start()
    {
        // SendEvent("Debug", "AnalyticsScript", "INitialized_analytics " + "script");
    }

    public void SendEvent(string eventName, string parameterName, object parameterValue)
    {
        try
        {
            FirebaseAnalytics.LogEvent(eventName, parameterName, parameterValue.ToString());
            Debug.Log("yolo analytics working");
        }
        catch (Exception e)
        {
            // LogToGUI.Log("Analytics::Error::Failed to send firebase event. " + e.Message);
        }
        try
        {
            Analytics.CustomEvent(eventName, new Dictionary<string, object> { { parameterName, parameterValue } });
        }
        catch (Exception e)
        {
            //LogToGUI.Log("Analytics::Error::Failed to send unity event. " + e.Message);
        }
    }


}
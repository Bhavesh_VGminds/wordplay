﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Text;
// using Gamelogic.Words.Dictionaries;
// using Gamelogic.Words;

public class WordRegion : MonoBehaviour
{
    public TextPreview textPreview;
    public Transform _Col_Container, _Row_Container;
    public Compliment compliment;
    private List<string> validWords = new List<string>();
    private GameLevel gameLevel;
    private int numWords, numCol, numRow;

    private RectTransform rt;
    public static WordRegion instance;

    bool isFound = false;
    public Transform _Cell;

    private List<string> extraList;
    private List<GameObject> RowList = new List<GameObject>();
    private List<GameObject> CellList = new List<GameObject>();
    private List<GameObject> ColList = new List<GameObject>();
    private List<LineWord> lines = new List<LineWord>();

    private void Awake()
    {
        instance = this;
        rt = GetComponent<RectTransform>();
    }
    void Start()
    {

    }

    public void Load(GameLevel gameLevel)
    {
        this.gameLevel = gameLevel;
        Debug.Log("Word List :" + this.gameLevel.answers.ToString());
        var wordList = CUtils.BuildListFromString<string>(this.gameLevel.answers.Replace("\"", string.Empty).Trim());
        extraList = CUtils.BuildListFromString<string>(this.gameLevel.validWords.Replace("\"", string.Empty).Trim());
        numWords = wordList.Count;

        var SortedString = wordList.OrderBy(x => x.Length);
        List<string> SortedList = SortedString.ToList();

        numCol = SortedList.Count <= 7 ? 1 :
                    SortedList.Count <= 14 ? 2 : 3;

        numRow = (int)Mathf.Ceil(SortedList.Count / (float)numCol);


        for (int j = 0; j < numCol; j++)
        {
            Transform Col_Container = Instantiate(_Col_Container);
            ColList.Add(Col_Container.gameObject);
            Col_Container.SetParent(this.transform);
            Col_Container.localScale = new Vector3(1, 1, 1);

            Col_Container.GetComponent<RectTransform>().sizeDelta = new Vector2(this.GetComponent<RectTransform>().rect.height, this.GetComponent<RectTransform>().rect.height);
        }

        int WordCounter = 0;
        string[] levelProgress = GetLevelProgress();
        int lastWordInCol = SortedList[SortedList.Count - 1].Length;

        for (int i = 0; i < numCol; i++)
        {
            int finalRow = 0;

            if (numCol == 3)
            {
                finalRow = (i == 0 || i == 1) ? numRow : (SortedList.Count - (numRow * 2));
            }
            else
            {
                if (numCol == 2)
                {
                    finalRow = (i == 0) ? numRow : (SortedList.Count - numRow);
                }
                else
                {
                    finalRow = numRow;
                }
            }

            for (int j = 0; j < finalRow; j++)
            {
                Transform row = Instantiate(_Row_Container);
                RowList.Add(row.gameObject);
                row.SetParent(ColList[i].transform);
                row.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
                lines.Add(row.gameObject.GetComponent<LineWord>());
                row.gameObject.GetComponent<LineWord>().lineNo = j;
                if (WordCounter == SortedList.Count)
                    WordCounter = SortedList.Count - 1;

                row.name = SortedList[WordCounter].ToUpper();
                row.GetComponent<LineWord>().answer = SortedList[WordCounter].ToUpper();
                if (levelProgress.Length > 0)
                {
                    row.GetComponent<LineWord>().SetProgress(levelProgress[j]);
                }
                char[] wordofAlpha = SortedList[WordCounter].ToCharArray();
                for (int c = 0; c < wordofAlpha.Length; c++)
                {
                    Cell cell = Instantiate(MonoUtils.instance.cell);
                    cell.GetComponent<RectTransform>().SetParent(row);
                    cell.name = wordofAlpha[c].ToString();
                    cell.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
                    cell.letter = wordofAlpha[c].ToString().ToUpper();

                    row.GetComponent<LineWord>().cells.Add(cell.GetComponent<Cell>());

                    // if (CUtils.getLastGameFlag(j.ToString())){
                    //     row.GetComponent<LineWord>().isShown = true;
                    //     cell.ShowText();
                    // }
                    // else{
                    row.GetComponent<LineWord>().isShown = false;
                    // }
                    CellList.Add(cell.gameObject);
                }
                WordCounter++;
            }
        }

        //Set coins inside the grid
        if (gameLevel.coins != "")
        {
            string[] Coin_Word = gameLevel.coins.Replace("\"", string.Empty).Trim().Split(',');

            if (Coin_Word.Length > 0)
            {
                if (!Prefs.IsSpecialCellOverlay)
                {
                    MainController.instance._overlay_Cell.gameObject.SetActive(true);
                    MainController.instance.isOverlayActive = true;
                    Prefs.IsSpecialCellOverlay = true;
                }
                for (int i = 0; i < Coin_Word.Length; i++)
                {
                    Cell[] cellObj = lines[int.Parse(Coin_Word[i])].GetComponentsInChildren<Cell>();

                    for (int j = 0; j < cellObj.Length; j++)
                    {
                        if (CUtils.getLastGameFlag(Coin_Word[i].ToString()) || !Prefs.IsLastLevel())
                        {
                            // cellObj[j].GetComponent<Cell>().ShowText();
                            cellObj[j].GetComponent<Cell>().coin.enabled = false;
                        }
                        else
                        {
                            cellObj[j].GetComponent<Cell>().coin.enabled = true;
                        }
                    }
                }
            }
        }

        float Cell_Size = 60;
        int NumberOfCellInLastRow = 0;
        int GSpace = 3, HSpace = 25;

        for (int i = 0; i < ColList.Count; i++)
        {
            var HList = ColList[i].GetComponentsInChildren<HorizontalLayoutGroup>().ToList();
            var HG = HList.Last();
            NumberOfCellInLastRow += HG.GetComponentsInChildren<Cell>().Length;
        }

        // Debug.Log("numberofCellInLastROw :" + NumberOfCellInLastRow.ToString());
        // Debug.Log("Row :" + numRow.ToString());
        if (numCol > 1)
        {
            if (NumberOfCellInLastRow == 7 || NumberOfCellInLastRow == 8)
            {
                Cell_Size = 65;
                HSpace = 15;
            }
            else if (NumberOfCellInLastRow == 9 || NumberOfCellInLastRow == 10)
            {
                Cell_Size = 55;
                HSpace = 15;
            }
            else if (NumberOfCellInLastRow == 11 || NumberOfCellInLastRow == 12)
            {
                Cell_Size = 50;
                HSpace = 15;
            }

            else if (NumberOfCellInLastRow == 13 || NumberOfCellInLastRow == 14)
            {
                Cell_Size = 40;
                HSpace = 6;
            }

            else if (NumberOfCellInLastRow == 15)
            {
                Cell_Size = 38;
                HSpace = 8;
            }

            else if (NumberOfCellInLastRow == 16)
            {
                Cell_Size = 35;
                HSpace = 5;
            }

            else if (NumberOfCellInLastRow == 17 || NumberOfCellInLastRow == 18)
            {
                Cell_Size = 32;
                HSpace = 5;
            }
            else
                Cell_Size = 60;

            if (NumberOfCellInLastRow < 13)
                Cell_Size -= HSpace;

            if (NumberOfCellInLastRow >= 15)
                this.GetComponent<HorizontalLayoutGroup>().spacing = 13;
            else
                this.GetComponent<HorizontalLayoutGroup>().spacing = Cell_Size;

            for (int i = 0; i < RowList.Count; i++)
            {
                RowList[i].GetComponent<HorizontalLayoutGroup>().spacing = HSpace;
            }

        }
        else
        {
            GSpace = 15;

            float cellwidth = (ColList[0].transform.parent.GetComponent<RectTransform>().rect.width / NumberOfCellInLastRow) - ((NumberOfCellInLastRow - 1) * ((NumberOfCellInLastRow < 5) ? HSpace : HSpace = 15));
            float cellheight = ColList[0].transform.parent.GetComponent<RectTransform>().rect.height / numRow - ((NumberOfCellInLastRow < 5) ? GSpace : GSpace = 15);
            Debug.Log("Height : :" + cellheight.ToString() + " Width:" + cellwidth.ToString());
            ColList[0].GetComponent<VerticalLayoutGroup>().spacing = GSpace;

            for (int i = 0; i < RowList.Count; i++)
            {
                RowList[i].GetComponent<HorizontalLayoutGroup>().spacing = (NumberOfCellInLastRow < 5) ? HSpace : HSpace = 15;
            }

            if (cellheight < cellwidth)
                Cell_Size = cellheight;
            else
                Cell_Size = (cellwidth < 0) ? (cellheight - cellwidth) : cellwidth;

            Cell_Size = Mathf.Abs(Cell_Size);
        }

        Debug.Log("Height : :" + Cell_Size.ToString() + " Width:" + Cell_Size.ToString());


        for (int i = 0; i < CellList.Count; i++)
        {
            CellList[i].GetComponent<RectTransform>().sizeDelta = new Vector2(Cell_Size, Cell_Size);
        }

    }

    public void CheckAnswer(string checkWord)
    {
        LineWord line = lines.Find(x => x.answer == checkWord);
        if (line != null)
        {
            if (!line.isShown)
            {
                float delayTime = 1f;
                Pan.instance.hand.gameObject.SetActive(false);
                MainController.instance.overlayLevel1.gameObject.SetActive(false);

                textPreview.SetAnswerColor();
                line.ShowAnswer();

                Prefs.totalWordFindAdded = Prefs.totalWordFindAdded + 1;
                if (Prefs.totalWordFindAdded % 2 == 0)
                {
                    delayTime = 1.5f;
                    compliment.ShowRandom();
                }

                CheckGameComplete(delayTime);
                Sound.instance.Play(Sound.Others.Alphabets, 0.5f);
            }
            else
            {
                line.ShowAnswer();
                textPreview.SetExistColor();
            }
        }
        else if (btnSearch(checkWord.ToLower()))
        {
            ExtraWord.instance.ProcessWorld(checkWord);
        }
        else if (checkWord.Length != 0)
        {
            textPreview.SetWrongColor();

        }

        textPreview.FadeOut();
    }

    private bool btnSearch(string searchString)
    {
        if (searchString != "")
        {
            if (extraList.Contains(searchString))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    private void CheckGameComplete(float dtime)
    {
        SaveLevelProgress();

        var isNotShown = lines.Find(x => !x.isShown);
        if (isNotShown == null)
        {
            if (Prefs.IsLastLevel())
                CurrencyController.CreditBalance(ConfigController.instance.GAME_CONFIG.coinConfig.word_cleared);

            ClearLevelProgress();
            // DOTweenManager.DisableTween(MainController.instance.btnShare.transform);
            DOTweenManager.DisableTween(MainController.instance.btnHint.transform);

            MainController.instance.OnComplete(dtime);

        }
    }

    public void HintClick()
    {
        int ballance = CurrencyController.GetBalance();
        if (ballance >= Const.HINT_COST)
        {

            var line = lines.Find(x => !x.isShown);

            if (line != null)
            {
                line.ShowHint();
                CurrencyController.DebitBalance(Const.HINT_COST);
                CheckGameComplete(1f);

                Prefs.AddToNumHint(Prefs.currentWorld, Prefs.currentSubWorld, Prefs.currentLevel);
                Prefs.totalHintAdded++;

                Sound.instance.Play(Sound.Others.Hint, 0.2f);
                AnalyticsScript.Instance.SendEvent("Use hint at word " + Prefs.currentLevel.ToString() + " of World " + Prefs.currentSubWorld.ToString(), "AnalyticsScript", "INitialized_analytics " + "WordRegion");

            }
        }
        else
        {
            DialogController.instance.ShowDialog(DialogType.Shop);
        }
    }

    public void SaveLevelProgress()
    {
        if (!Prefs.IsLastLevel()) return;

        List<string> results = new List<string>();
        foreach (var line in lines)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var cell in line.cells)
            {
                sb.Append(cell.isShown ? "1" : "0");
            }
            results.Add(sb.ToString());
        }

        CryptoPlayerPrefsX.SetStringArray("level_progress", results.ToArray());
    }

    public string[] GetLevelProgress()
    {
        // Debug.Log("Is It Last level :"+Prefs.IsLastLevel());
        if (!Prefs.IsLastLevel()) return new string[0];
        return CryptoPlayerPrefsX.GetStringArray("level_progress");
    }

    public void ClearLevelProgress()
    {
        if (!Prefs.IsLastLevel()) return;
        CPlayerPrefs.DeleteKey("level_progress");
    }
}

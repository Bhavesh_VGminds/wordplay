﻿using UnityEngine;

[System.Serializable]
public class GameLevel : ScriptableObject
{
    public string wordNumber;
    public string word;
    public string answers;
    public string validWords;
    public string specialChar;
    public string coins;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class DOTweenManager{
	
	public static void ArrayObjectAnim_MoveX(Transform[] Obj,bool Flip=false){
		for(int i=0;i<Obj.Length;i++){
			if(Flip)
				Obj[i].DOLocalMoveX(-500,0.5f).SetEase(Ease.OutBounce).Flip();
			else
				Obj[i].DOLocalMoveX(-500,0.3f).From();

			if(!Obj[i].gameObject.activeInHierarchy)
				Obj[i].gameObject.SetActive(true);
		}
	}

	public  static void ButtonActiveEffect(Transform transform){
		transform.DOScale(new Vector3(1.1f,1.1f,1.1f),1).SetEase(Ease.OutSine).SetLoops(-1,LoopType.Yoyo).From();
	}

	public  static void DisableTween(Transform transform){
		transform.DOKill(true);
	}

	public static void CellShaking(Transform transform){
		transform.transform.DOShakeScale(1,0.2f).From();
	}

}

using System;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using UnityEngine;

namespace AdServers
{
    namespace AppodealAds
    {
        public class AppodealReward : MonoBehaviour, IAppodealReward,IRewardedVideoAdListener
        {
            private Action onAdComplete;

            private bool testReward;
            public void ShowAd(Action OnAdComplete)
            {
        
                this.OnAdComplete = OnAdComplete;
                testReward = true;
                Debug.Log("ShowAdReward");
                Appodeal.show(Appodeal.REWARDED_VIDEO);
            }

            public Action OnAdComplete
            {
                get
                {
                    return onAdComplete;
                }
                set
                {
                    onAdComplete = value;
                }
            }

            public bool IsAdReady()
            {
                return true;
            }

            public void OnAdFinished()
            {
                Debug.Log("OnAdClosed");
                if (OnAdComplete != null)
                {
                    OnAdComplete.Invoke();
                }
            }

            public void onRewardedVideoLoaded()
            {
                print("reward loaded");
                throw new NotImplementedException();
            }

            public void onRewardedVideoFailedToLoad()
            {
                print("reward not loaded");
                throw new NotImplementedException();
            }

            public void onRewardedVideoShown()
            {
                print("reward shown");
                throw new NotImplementedException();
            }

            public void onRewardedVideoFinished(int amount, string name)
            {
                print("reward finished");                
            }

            public void onRewardedVideoClosed(bool finished)
            {
                print("reward closed");
                OnAdFinished();
            }

            //Small trick to see if the reward ad is closed with application pause function
            void OnApplicationPause(bool isPause)
            {
                if(testReward && !isPause)
                {
                    testReward = false;
                    OnAdFinished();
                }
            }
        }
    }
}
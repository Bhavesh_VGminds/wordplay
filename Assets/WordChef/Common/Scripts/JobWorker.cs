﻿#define NO_GPS
using UnityEngine;
//using GooglePlayGames;
using System;

public class JobWorker : MonoBehaviour
{
    public Action<string> onEnterScene;
    public Action onLink2Store;
    public Action onDailyGiftReceived;
    public Action onShowBanner;
    public Action onCloseBanner;
    public Action onShowFixedBanner;
    public Action onShowInterstitial;

    public static JobWorker instance;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
#if !NO_GPS
        PlayGamesPlatform.Activate();
#endif
    }

#if !NO_GPS
    // public void ShowLeaderboard()
    // {
    //     Social.localUser.Authenticate((bool success) => {
    //         if (success)
    //         {
    //             PlayGamesPlatform.Instance.ShowLeaderboardUI(GPGSIds.leaderboard_high_score);
    //             GoogleAnalyticsV3.instance.LogEvent("Buttons", "Leaderboard", "Leaderboard Show", 0);
    //         }
    //     });

    //     GoogleAnalyticsV3.instance.LogEvent("Buttons", "Leaderboard", "Leaderboard Click", 0);
    // }

    // public void ReportScore(int score)
    // {
    //     if (Social.localUser.authenticated)
    //     {
    //         Social.ReportScore(score, GPGSIds.leaderboard_high_score, (bool postSuccess) => {
    //             // handle success or failure
    //         });
    //     }
    // }
#endif



}
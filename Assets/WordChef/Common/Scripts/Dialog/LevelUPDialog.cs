﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LevelUPDialog : Dialog
{

    public Text txtMsg, txtTitle, noOfCoin, txtClaim;
    public ParticleSystem p1, p2;
    int coin = 0;

    protected override void Start()
    {
        base.Start();
        Sound.instance.PlayButton();
        coin = ConfigController.instance.Config().coinConfig.level_cleared;
        noOfCoin.text = coin.ToString();
        string currentLevel = ((Prefs.currentLevel == 0) ? 1 : Prefs.currentLevel).ToString();
        txtTitle.text = string.Format(txtTitle.text, currentLevel);
        txtMsg.text = string.Format(txtMsg.text, currentLevel, Prefs.currentWorldName);
        txtClaim.text = (Prefs.IsLastLevel()) ? "Claim" : "Ok";

        p1.Play();
        p2.Play();
    }
    public void btnClaim()
    {
        Sound.instance.PlayButton();
        if (Prefs.IsLastLevel())
            ConfigController.instance.CreateFlyingCoin(noOfCoin.transform.parent.position, coin);
        Invoke("Closebtn", 1.2f);
    }

    void Closebtn()
    {
        base.Close();
        CUtils.LoadScene((Prefs.currentWorld > 0) ? 0 : 2, true);
    }
}
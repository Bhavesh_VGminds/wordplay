﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class Worlds : MonoBehaviour
{
    public Image BadgeBG, PlayButtonBG;
    public Color cActiveColor, cDisableColor;
    public SpriteRenderer Badge;
    public Transform isDone;
    public Text WorldName, LevelName, WorldProgressText;
    public int Index;
    public string TotalLevel;

    public Outline OutLine;

    public Toggle WorldToggle;

    void Start()
    {
        this.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        this.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
        WorldToggle.interactable = true;
    }

    public void LevelContainer()
    {
        if (Index > Prefs.unlockedWorld)
        {
            LevelName.text = "Level - 0 / " + TotalLevel;
            PlayButtonBG.color = cDisableColor;
            SetToggle(WorldToggle);
        }
        else if (Index <= Prefs.unlockedWorld)
        {
            WorldToggle.group = this.transform.parent.GetComponent<ToggleGroup>();
            PlayButtonBG.color = cActiveColor;
            LevelName.text = "Level - " + (Prefs.unlockedSubWorld + 1).ToString() + " /" + TotalLevel;

            if (Index == Prefs.unlockedWorld)
            {
                DOTweenManager.ButtonActiveEffect(isDone);
                WorldController.instence.scrollRect.DOVerticalNormalizedPos((1 - (Prefs.unlockedWorld / 10)), 1).SetDelay(0.2f);
            }

            Transform lvlParent = Instantiate(WorldController.instence._levelsParent) as Transform;
            lvlParent.name = Index.ToString();
            WorldController.instence.levelsList.Add(lvlParent.GetComponent<LayoutElement>());
            lvlParent.SetParent(WorldController.instence._categoryContainer);
            lvlParent.localScale = new Vector3(1, 1, 1);
            lvlParent.localPosition = Vector3.Normalize(Vector3.one);
            SetToggle(WorldToggle, lvlParent.gameObject);

            WorldController.instence.CreateLevel(Index, lvlParent);

            if (Index < Prefs.unlockedWorld || Prefs.unlockedWorld == WorldController.instence.totalWorld)
            {
                LevelName.text = "Level - " + TotalLevel + "/" + TotalLevel;
                WorldProgressText.text = "Done";
                PlayButtonBG.color = cActiveColor;
                isDone.gameObject.SetActive(true);

            }
            else
            {
                LevelName.text = "Level - " + Prefs.unlockedSubWorld.ToString() + "/" + TotalLevel;
                WorldProgressText.text = "Play";
              //  PlayButtonBG.color = cDisableColor;
                Badge.sortingLayerName = "UI";
                Badge.sortingOrder = 0;
                isDone.gameObject.SetActive(false);

            }
        }
    }

    void SetToggle(Toggle toggle, GameObject childObject = null)
    {
        toggle.onValueChanged.RemoveAllListeners();
        toggle.onValueChanged.AddListener((abc) => OnValueChangeToggle(childObject));
        if (childObject != null)
            childObject.SetActive(false);
    }

    void OnValueChangeToggle(GameObject isActiveObject = null)
    {

        Sound.instance.PlayButton();
        if (isActiveObject != null)
        {
            if (isActiveObject.activeInHierarchy)
            {
                isActiveObject.transform.DOScaleX(0, 0.2f).OnComplete(() => HideLevels(isActiveObject));
            }
            else
            {
                isActiveObject.transform.DOScaleX(0, 0.2f).From().OnUpdate(() => ShowLevels(isActiveObject));
                if (int.Parse(isActiveObject.name) > 5)
                    GetComponent<ScrollRect>().DOVerticalNormalizedPos((1 - ((Prefs.unlockedWorld > 10) ? 0.8f : Prefs.unlockedWorld / 10)), 1).SetDelay(0.2f);
            }
        }
        else
        {
            Toast.instance.ShowMessage("First clear above categories. ");
        }

    }

    private void ShowLevels(GameObject categoryObj)
    {
        categoryObj.SetActive(true);
    }

    private void HideLevels(GameObject categoryObj)
    {
        categoryObj.SetActive(false);
        categoryObj.transform.localScale = Vector3.one;

    }


}

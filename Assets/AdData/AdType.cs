﻿namespace VGM
{
	namespace Ads
	{
		public enum AdType
		{
			None,
			Banner_Top,
			Banner_Bottom,
			Reward,
			Interstitial,
			Flexy,
			Panel
		}
	}
}
﻿using UnityEngine;
using System.Collections;

public class PlatformGameVersion
{
    public enum Platform
    {
        Android,
        Ios
    };

    public Platform PlatformType { get; set; }

    public string Version { get; set; }

    public string StoreUrl { get; set; }

    public bool ForceUpdate { get; set; }

    public string Message { get; set; }
}

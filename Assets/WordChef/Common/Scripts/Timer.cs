﻿using UnityEngine;
using System.Collections;

public class Timer{
    private static MonoBehaviour behaviour;
    public delegate void Task();

    public static void Schedule(MonoBehaviour _behaviour, float delay, Task task)
    {
        if(_behaviour){
               behaviour = _behaviour.GetComponent<MonoBehaviour>();

        behaviour.StartCoroutine(DoTask(task, delay));
        }
     
    }

    private static IEnumerator DoTask(Task task, float delay)
    {
        yield return new WaitForSeconds(delay);
        task();
    }
}

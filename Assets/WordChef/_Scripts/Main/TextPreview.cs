﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using DG.Tweening;
public class TextPreview : MonoBehaviour
{
    public GameObject content;
    public RectTransform backgroundRT, textRT;
    public string word;
    public Text text;

    [Header("")]
    public Color answerColor;
    public Color validColor;
    public Color wrongColor;
    public Color existColor;
    public Color defaultColor;

    public int TryCounter = 0;

    public static TextPreview instance;

    private void Awake()
    {
        instance = this;
    }

    public void SetIndexes(List<int> indexes)
    {
        StringBuilder sb = new StringBuilder();
        foreach (var i in indexes)
        {
            sb.Append(word[i]);
        }
        text.text = sb.ToString();
        backgroundRT.sizeDelta = new Vector2(textRT.GetComponent<Text>().preferredWidth + 50, backgroundRT.parent.GetComponent<RectTransform>().sizeDelta.y);
    }

    public void ShowWordsHits()
    {
        SetActive(true);
        backgroundRT.GetComponent<Image>().enabled = false;
        textRT.GetComponent<Outline>().enabled = false;
        text.GetComponent<Text>().fontSize = 55;
        if (MainController.instance.isOverlayActive)
            text.GetComponent<Text>().color = Color.white;
        else
            text.GetComponent<Text>().color = Color.white;
    }

    public void SetActive(bool isActive)
    {
        content.SetActive(isActive && text.text.Length > 0);
    }

    public void ClearText()
    {
        text.text = "";
    }

    public void SetText(string textStr)
    {
        text.text = textStr;
    }

    public string GetText()
    {
        return text.text;
    }

    public void SetAnswerColor()
    {
        backgroundRT.GetComponent<Image>().color = answerColor;
        TryCounter = 0;

        Sound.instance.Play(Sound.Others.Alphabets);
    }

    public void SetValidColor()
    {
        backgroundRT.GetComponent<Image>().color = validColor;
        TryCounter = 0;
    }

    public void SetWrongColor()
    {
        backgroundRT.GetComponent<Image>().color = wrongColor;
        backgroundRT.parent.DOShakePosition(1, 4);

        TryCounter++;
        Debug.Log("TryCounter :" + TryCounter.ToString());
        if (TryCounter == 1)
        {


            MainController.instance.btnHint.GetComponent<Animator>().enabled = true;
            DOTweenManager.DisableTween(MainController.instance.btnHint.transform);
        }
        else if (TryCounter == 3)
        {
            MainController.instance.btnHint.GetComponent<Animator>().enabled = false;
            DOTweenManager.ButtonActiveEffect(MainController.instance.btnHint.transform);
        }
        // else if (TryCounter > 4 && Prefs.currentWorld > 0)
        // {
        //     if (PlayerPrefs.GetInt("isShareOverlayShow", 0) == 0)
        //     {
        //         PlayerPrefs.SetInt("isShareOverlayShow", 1);
        //         MainController.instance.isOverlayActive = true;
        //         MainController.instance._overlay_share.gameObject.SetActive(true);
        //     }

        // }


        Sound.instance.Play(Sound.Others.WordNotFound);
    }

    public void SetDefaultColor()
    {
        backgroundRT.GetComponent<Image>().color = defaultColor;
    }

    public void SetExistColor()
    {
        backgroundRT.GetComponent<Image>().color = existColor;
        Sound.instance.Play(Sound.Others.WordAlreadyFound);
    }

    public void FadeOut()
    {
        iTween.ValueTo(gameObject, iTween.Hash("from", 1, "to", 0, "time", 0.4f, "onupdate", "OnUpdate", "ontarget", gameObject));
    }

    public void FadeIn()
    {
        backgroundRT.GetComponent<Image>().enabled = true;
        //textRT.GetComponent<Outline>().enabled = true;
        text.GetComponent<Text>().fontSize = 85;
        text.GetComponent<Text>().color = Color.white;
        iTween.Stop(gameObject);
        SetDefaultColor();
        content.GetComponent<CanvasGroup>().alpha = 1;
    }

    private void OnUpdate(float value)
    {
        content.GetComponent<CanvasGroup>().alpha = value;
    }
}

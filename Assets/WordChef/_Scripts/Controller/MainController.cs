﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Superpow;
using DG.Tweening;
using VGMTools.Ads.Manager;
using VGMTools.Ads.Properties;
using VGMTools.Ads.Relations.Shared;
public class MainController : BaseController
{
    public Text levelNameText;
    public GameObject btnHint, btnSuffle, VideoAdsButton;
    public GameObject btnExtraWords;
    public GameObject btnMission;
    public bool isOverlayActive = false;
    public Image OverLay, overlayLevel1, _overlay_Cell, _overlay_SpecialChar, _overlay_share;
    public Image overlayExtraWord;
    private int world, subWorld, level;
    private bool isGameComplete;
    private GameLevel gameLevel;

    public int showBannerAfterWorld = 0, showInertitialAfterEveryWord = 2;

    public static MainController instance;

    protected override void Awake()
    {
        base.Awake();
        instance = this;
    }

    protected override void Start()
    {
        base.Start();
        world = Prefs.currentWorld;
        subWorld = Prefs.currentSubWorld;
        level = Prefs.currentLevel;

        JsonToClass.gameDataFileName = "game-json.json";
        JsonToClass.LoadGameData();

        gameLevel = JsonToClass.getWorldLevel(world, subWorld, level);
        Pan.instance.Load(gameLevel);
        WordRegion.instance.Load(gameLevel);


        if (Prefs.extraProgress > 0)
        {
            btnExtraWords.SetActive(true);
            btnExtraWords.transform.Find("Panel").GetComponentInChildren<Text>().text = Prefs.extraProgress.ToString() + "/" + Prefs.extraTarget.ToString();
        }
        else
        {
            btnExtraWords.SetActive(false);
        }

        if (Prefs.currentWorld == 0 && Prefs.currentSubWorld == 0 && Prefs.currentLevel == 4 && Prefs.IsLastLevel() && Prefs.LastdayNumber != Prefs.GetTotalDaysOfLogin)
        {
            DialogController.instance.ShowDialog(DialogType.DailyBonus);
        }
        else if (Prefs.unlockedWorld > 0 && Prefs.LastdayNumber != Prefs.GetTotalDaysOfLogin)
        {
            DialogController.instance.ShowDialog(DialogType.DailyBonus);
        }

        if (Prefs.unlockedWorld == 0 && Prefs.unlockedSubWorld == 0 && Prefs.unlockedLevel == 0 && Prefs.IsLastLevel())
        {
            isOverlayActive = true;
            overlayLevel1.gameObject.SetActive(true);
            btnExtraWords.SetActive(false);
            btnHint.SetActive(false);
            btnSuffle.SetActive(false);
        }
        else if (Prefs.unlockedWorld == 0 && Prefs.unlockedSubWorld == 0 && Prefs.unlockedLevel == 1 && Prefs.IsLastLevel())
        {
            Debug.Log("### :" + Prefs.currentLevel.ToString() + " " + Prefs.currentSubWorld.ToString() + " " + Prefs.currentLevel.ToString());
            isOverlayActive = true;
            OverLay.gameObject.SetActive(true);
            btnExtraWords.SetActive(false);
        }
        if (Prefs.LastWorld == ((Prefs.unlockedWorld == 1) ? Prefs.unlockedWorld = 1 : Prefs.unlockedWorld / 2) && Prefs.unlockedLevel == 0 && Prefs.unlockedSubWorld == 0 && Prefs.IsLastLevel() && !Prefs.IsRateUs)
        {
            Debug.Log("Rate Us !");
            Timer.Schedule(this, 0.5f, () =>
            {
                DialogController.instance.ShowDialog(DialogType.RateUs);
            });
        }

        //Interstitial Ads
        if (Prefs.currentWorld > showBannerAfterWorld && (Prefs.currentLevel + 1) % showInertitialAfterEveryWord == 0)
        {
            CUtils.ShowInterstitialAd();
        }

        Prefs.currentWorldName = JsonToClass.GetCategories()[Prefs.currentWorld].cn;
        Prefs.currentSubWorldName = "Level " + (Prefs.currentSubWorld + 1).ToString();
        levelNameText.text = Prefs.currentWorldName;
        WordRegion.instance.textPreview.text.text = "Solved by" + ConfigController.instance.GetWordHits(int.Parse(gameLevel.wordNumber) - 1) + "% " + "Players";
        WordRegion.instance.textPreview.ShowWordsHits();

        CurrencyController.onBalanceChanged += OnBalanceChanged;
    }

    public void OnComplete(float delayTime)
    {
        if (isGameComplete) return;
        isGameComplete = true;

        Timer.Schedule(this, delayTime, () =>
        {
            DialogController.instance.ShowDialog(DialogType.Win);
            Sound.instance.Play(Sound.Others.WordUp);
        });
    }

    private string BuildLevelName()
    {
        return world + "-" + subWorld + "-" + level;
    }

    public float speed = 0.1F;
    void Update()
    {
        int nbTouches = Input.touchCount;
        if (nbTouches > 0 || Input.GetMouseButtonDown(0))
        {
            isOverlayActive = false;
            OverLay.gameObject.SetActive(false);
            overlayExtraWord.gameObject.SetActive(false);
            overlayLevel1.gameObject.SetActive(false);
            _overlay_Cell.gameObject.SetActive(false);
            _overlay_SpecialChar.gameObject.SetActive(false);
            _overlay_share.gameObject.SetActive(false);
        }
    }


    void OnBalanceChanged()
    {
        if (CurrencyController.GetBalance() < 200 && Prefs.currentWorld > 0 && AdManager.IsAdReady<IRewardTag>())
        {
            VideoAdsButton.GetComponent<Image>().DOFade(1, 1);
            DOTweenManager.ButtonActiveEffect(VideoAdsButton.transform);
        }
        else
        {
            VideoAdsButton.GetComponent<Image>().DOFade(0, 1);
            DOTweenManager.DisableTween(VideoAdsButton.transform);
        }
    }

    public void ShowRewardAds()
    {
        AdManager.ShowAd<IRewardTag, System.Action>(CallBack);
    }

    void CallBack()
    {
        DialogController.instance.ShowDialog(DialogType.RewardedVideo);
    }

    void OnDisable()
    {
        CurrencyController.onBalanceChanged -= OnBalanceChanged;
    }

}

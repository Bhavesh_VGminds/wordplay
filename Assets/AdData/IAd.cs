﻿namespace VGM
{
	namespace Ads
	{
		public interface IAd
		{
			void Show ();

			void Hide ();
		}
	}
}
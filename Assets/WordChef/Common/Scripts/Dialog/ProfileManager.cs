﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
public class ProfileManager : Dialog
{

    public ToggleGroup toggleGroup;
    public Image HeaderProfileImage;

	public Text LevelCompleted,TimeSpentOnLevel,WordFound,HiddenWordsFound,HintUsed;
    Toggle[] toggleArray = null;
    protected override void Start()
    {
        base.Start();
        toggleArray = toggleGroup.GetComponentsInChildren<Toggle>();
        HeaderProfileImage.sprite = Resources.Load("Avatars/" + Prefs.PlayerProfileImage, typeof(Sprite)) as Sprite;
        for (int i = 0; i < toggleArray.Length; i++)
        {
            toggleArray[i].GetComponent<Image>().color = Color.white;
			 Image[] image = toggleArray[i].gameObject.GetComponentsInChildren<Image>();
			if(image[1].sprite.name==Prefs.PlayerProfileImage){
				 image[0].color = new Color32(146, 255, 0, 255);
			}else{
				 toggleArray[i].GetComponent<Image>().color = Color.white;
			}
        }

		//Display Data
		LevelCompleted.text=Prefs.TotalUplockedLevelAdded.ToString();
		WordFound.text=Prefs.totalWordFindAdded.ToString();
		HiddenWordsFound.text=Prefs.totalExtraAdded.ToString();
		HintUsed.text=Prefs.totalHintAdded.ToString();

    }

    public void OnToggleChange()
    {
        for (int i = 0; i < toggleArray.Length; i++)
            toggleArray[i].GetComponent<Image>().color = Color.white;

        IEnumerable<Toggle> toggles = toggleGroup.ActiveToggles();
        var activeToggle = toggles.FirstOrDefault<Toggle>();
        Image[] image = activeToggle.gameObject.GetComponentsInChildren<Image>();
        image[0].color = new Color32(146, 255, 0, 255);
        Prefs.PlayerProfileImage = image[1].sprite.name;
        HeaderProfileImage.sprite = image[1].sprite as Sprite;
        GameObject.FindGameObjectWithTag("MissionProfileImage").GetComponent<Image>().sprite = image[1].sprite as Sprite;
    }


}

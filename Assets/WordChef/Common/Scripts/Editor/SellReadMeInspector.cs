﻿using System;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SellReadMe))]
public class SellReadMeInspector : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("1. Edit Game Settings (Admob, In-app Purchase..)", EditorStyles.boldLabel);

        if (GUILayout.Button("Edit Game Settings", GUILayout.MinHeight(40)))
        {
            Selection.activeObject = AssetDatabase.LoadMainAssetAtPath("Assets/WordChef/Common/Prefabs/GameMaster.prefab");
        }

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("2. Game Documentation", EditorStyles.boldLabel);
        if (GUILayout.Button("Set Up (Important)", GUILayout.MinHeight(40)))
        {
            Application.OpenURL("https://docs.google.com/document/d/1aymh-PnpVm7qpVVHIRitORgtvTUonYPwgF7nOe_UrKc/edit?usp=sharing");
        }

        EditorGUILayout.Space();
        if (GUILayout.Button("Open Full Documentation", GUILayout.MinHeight(40)))
        {
            Application.OpenURL("https://docs.google.com/document/d/1fUTEeuEcSsdFx_I9cX3wbktGeKtm8MWLggiqApnooec/edit?usp=sharing");
        }

        EditorGUILayout.Space();
        if (GUILayout.Button("Setup In-app Purchase Video Guide", GUILayout.MinHeight(40)))
        {
            Application.OpenURL("https://www.youtube.com/watch?v=LFuKHmFeR9g");
        }

        EditorGUILayout.Space();
        if (GUILayout.Button("Build For iOS Video Guide", GUILayout.MinHeight(40)))
        {
            Application.OpenURL("https://www.youtube.com/watch?v=M-ZndeOMwIs");
        }

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("3. My Other Great Source Codes", EditorStyles.boldLabel);
        if (GUILayout.Button("Hexa Puzzle Block", GUILayout.MinHeight(30)))
        {
            Application.OpenURL("https://www.assetstore.unity3d.com/en/#!/content/85474");
        }

        EditorGUILayout.Space();

        if (GUILayout.Button("Unblock Me", GUILayout.MinHeight(30)))
        {
            Application.OpenURL("https://www.assetstore.unity3d.com/en/#!/content/69070");
        }

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("4. Contact Us For Support", EditorStyles.boldLabel);
        EditorGUILayout.TextField("Email: ", "phuongdong0702@gmail.com");
        EditorGUILayout.TextField("Skype: ", "phuongdong0702");

    }
}
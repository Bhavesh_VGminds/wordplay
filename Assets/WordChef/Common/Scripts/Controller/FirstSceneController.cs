﻿using UnityEngine;
using System.Collections;

public class FirstSceneController : MonoBehaviour
{
	public static FirstSceneController instance;

	private void Awake()
	{
		instance = this;
		Application.targetFrameRate = 60;
        CPlayerPrefs.useRijndael(CommonConst.ENCRYPTION_PREFS);
	}

	private void Start(){
		CUtils.SetAndroidVersion(ConfigController.instance.GAME_CONFIG.versionConfig.ANDROID);
		CUtils.SetIOSVersion(ConfigController.instance.GAME_CONFIG.versionConfig.ANDROID);
		CUtils.SetWindowsPhoneVersion(ConfigController.instance.GAME_CONFIG.versionConfig.WP);	
		CUtils.SetHintCost(ConfigController.instance.GAME_CONFIG.coinConfig.hint_cost_coin);
	}

	private void Update()
    {
#if UNITY_ANDROID
        if (Input.GetKeyDown(KeyCode.Escape) && !DialogController.instance.IsDialogShowing())
		{
            Application.Quit();
        }
#endif
    }
}

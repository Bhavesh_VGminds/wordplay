﻿using System;
using UnityEngine;
using UnityEngine.Collections;
using UnityEngine.UI;
using DG.Tweening;
public static class CExtension
{
    public static void SetText(this GameObject obj, string value)
    {
        Text text = obj.GetComponent<Text>();
        string OldValue = text.text;
        if (text != null)
        {
            if (!Const.doScrumble)
                text.text = value;

            Const.doScrumble = false;
        }
    }

    public static void SetText(this Text objText, string value)
    {
        // objText.text = value;
        string OldValue = objText.text;
        if (value != null)
        {
            if (!Const.doScrumble)
            {
                objText.text = value;
            }
            // else
            //     objText.text = value;

            Const.doScrumble = false;

        }
    }

    public static void SetTimeText(this Text text, String preFix, int time)
    {
        TimeSpan t = TimeSpan.FromSeconds(time);
        text.text = preFix + string.Format("{0:D2}:{1:D2}", t.Minutes, t.Seconds);
    }
}

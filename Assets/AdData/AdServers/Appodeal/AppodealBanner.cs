using UnityEngine;
using AppodealAds.Unity.Common;
using AppodealAds.Unity.Api;
using System;
using VGMTools.Ads.Properties;

namespace AdServers
{
	namespace AppodealAds
	{
		public class AppodealBanner : MonoBehaviour, IAppodealBanner,IBannerAdListener
		{
			public bool IsAdReady ()
			{
				return Appodeal.isLoaded (Appodeal.BANNER);
			}

			public void ShowAd (BannerPos pos)
			{
				//LogToGUI.Log ("AppodealBanner::ShowAd::showing appodeal banner ad");
				Appodeal.show (pos == BannerPos.Top ? Appodeal.BANNER_TOP : Appodeal.BANNER_BOTTOM);
			}

			public void HideAd ()
			{
				Appodeal.hide (Appodeal.BANNER_BOTTOM);
				Appodeal.hide (Appodeal.BANNER_TOP);
			}

			#region IBannerAdListener implementation

			public void onBannerLoaded ()
			{
				throw new System.NotImplementedException ();
			}

			public void onBannerFailedToLoad ()
			{
				throw new System.NotImplementedException ();
			}

			public void onBannerShown ()
			{
				throw new System.NotImplementedException ();
			}

			public void onBannerClicked ()
			{
				throw new System.NotImplementedException ();
			}

            public void onBannerLoaded(bool isPrecache)
            {
                throw new NotImplementedException();
            }

            #endregion
        }
	}
}
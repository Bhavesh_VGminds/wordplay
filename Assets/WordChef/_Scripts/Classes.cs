﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;



[System.Serializable]
public class Category
{
    public string cn;
    public int p;
    public Levels[] levels;

    public Category() { }

    public Category(string name, int point, Levels[] levelObj)
    {
        cn = name;
        p = point;
        levels = levelObj;
    }
}

[System.Serializable]
public class Levels
{
    public string ln;
    public Words[] words;

    public Levels() { }

    public Levels(string name, Words[] wordObject)
    {
        ln = name;
        words = wordObject;
    }
}

[System.Serializable]
public class Words
{
    public string wn;
    public string w;
    public string s_char;
    public string cns;
    public string s;
    public string ex;

    public Words() { }
    public Words(string WN, string W, string S_CHAR, string CNS, string S, string EX)
    {
        this.wn = WN;
        this.w = W;
        this.s_char = S_CHAR;
        this.cns = CNS;
        this.s = S;
        this.ex = EX;
    }
}
[System.Serializable]
public class RootObject
{
    public List<Category> cat;
}
public class JsonToClass
{
    public static string gameDataFileName = "game-json.json";
    public static string dataAsJson = "";
    static RootObject rootObject;

    public static string getFullPath(string jsonFileName)
    {
        var RealFilePath = "";
#if UNITY_EDITOR
        RealFilePath = string.Format(@"Assets/StreamingAssets/{0}", jsonFileName);
#else
        // check if file exists in Application.persistentDataPath
         RealFilePath = string.Format("{0}/{1}", Application.persistentDataPath, jsonFileName);

        if (!File.Exists(RealFilePath))
        {
            Debug.Log("Database not in Persistent path");
            // if it doesn't ->
            // open StreamingAssets directory and load the db ->

#if UNITY_ANDROID
            var loadJson = new WWW("jar:file://" + Application.dataPath + "!/assets/" + jsonFileName);  // this is the path to your StreamingAssets in android
            while (!loadJson.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
            // then save to Application.persistentDataPath
            File.WriteAllBytes(RealFilePath, loadJson.bytes);
#elif UNITY_IOS
                 var loadJson = Application.dataPath + "/Raw/" + jsonFileName;  // this is the path to your StreamingAssets in iOS
                // then save to Application.persistentDataPath
                File.Copy(loadJson, RealFilePath);
#else
	var loadJson = Application.dataPath + "/StreamingAssets/" + jsonFileName;  // this is the path to your StreamingAssets in iOS
	// then save to Application.persistentDataPath
	File.Copy(loadJson, RealFilePath);

#endif

        
    }
#endif
        return RealFilePath;
    }


    public static void LoadGameData()
    {
        
        string filePath = getFullPath(gameDataFileName);

        if (File.Exists(filePath))
        {
            // Read the json from the file into a string
            dataAsJson = File.ReadAllText(filePath);
            rootObject = JsonUtility.FromJson<RootObject>(dataAsJson);
        }
        else
        {
            Debug.LogError("Cannot load game data!");
        }
    }

    public static Category[] GetCategories()
    {
        return rootObject.cat.ToArray();
    }

    public static Levels[] GetSection(int categoryNo)
    {
        return rootObject.cat[categoryNo].levels;
    }

    public static int GetTotalSection(int categoryNo)
    {
        return rootObject.cat[categoryNo].levels.Length;
    }

    public static Words[] GetLevels(int categoryNo, int sectionNo)
    {
        return rootObject.cat[categoryNo].levels[sectionNo].words;
    }

    public static GameLevel getWorldLevel(int categoryNo, int sectionNo, int wordNo)
    {
        // Debug.Log("World : " + categoryNo.ToString() + " Level : " + sectionNo.ToString() + " Word : " + wordNo.ToString());
        GameLevel gLevel = new GameLevel();
        Words wordsObject = new Words();
        wordsObject = rootObject.cat[categoryNo].levels[sectionNo].words[wordNo];
        gLevel.wordNumber=wordsObject.wn.Trim().Split('-')[2];
        gLevel.word = wordsObject.w.ToString();
        gLevel.specialChar = wordsObject.s_char;
        gLevel.coins = wordsObject.cns;
        gLevel.answers = wordsObject.s;
        gLevel.validWords = wordsObject.ex;

        return gLevel;
    }
}

[Serializable]
public class CoinIAP
{
    public string C_ID;
    public string Name;
    public string ImageName;
    public int Coin;
    public float Old_Price;
    public float New_Price;
    public string Offer;
    public string Tag;
}

public class IAPJsonClass
{
    public static string gameIAPDataFileName = "iap-json.txt";
    public static string jsonData = "";
    static SimpleJSON.JSONNode N;

    public static CoinIAP[] GetIAPData()
    {
        //		 Path.Combine combines strings into a file path
        //		 Application.StreamingAssets points to Assets/StreamingAssets in the Editor, and the StreamingAssets folder in a build
        string filePath = JsonToClass.getFullPath(gameIAPDataFileName);
        if (File.Exists(filePath))
        {
            // Read the json from the file into a string
            jsonData = File.ReadAllText(filePath);
            N = SimpleJSON.JSON.Parse(jsonData);
            JSONClass IAPjson = (JSONClass)N;
            Debug.Log(IAPjson.Count);
            CoinIAP[] IAPItems = new CoinIAP[IAPjson.Count];
            for (int i = 0; i < IAPItems.Length; i++)
            {
                // Debug.Log(IAPjson[i]["C_ID"]);
                IAPItems[i] = new CoinIAP();
                IAPItems[i].C_ID = (string)IAPjson[i]["C_ID"];
                IAPItems[i].Name=IAPjson[i]["Name"];
                IAPItems[i].Coin = IAPjson[i]["Coin"].AsInt;
                IAPItems[i].ImageName = IAPjson[i]["ImageName"];
                IAPItems[i].Old_Price = IAPjson[i]["Old_Price"].AsFloat;
                IAPItems[i].New_Price = IAPjson[i]["New_Price"].AsFloat;
                IAPItems[i].Offer = (string)IAPjson[i]["Offer"];
                IAPItems[i].Tag = IAPjson[i]["Tag"];
            }
            return IAPItems;
        }
        else
        {
            Debug.LogError("Cannot load game data!");
            return null;
        }

    }
}

[Serializable]
public class Mission
{
    public string Name;
    public string Icon_Name;
    public int InitialValue;
    public int incrementalValue;
    public int Coins;
    public string Mission_Message;
}

public class MissionJsonClass
{
    public static string gameIAPDataFileName = "Mission.json";
    public static string jsonData = "";
    static SimpleJSON.JSONNode N;
    public static string filePath;
    public MissionJsonClass()
    {
        filePath = JsonToClass.getFullPath(gameIAPDataFileName);
        if (File.Exists(filePath))
        {
            // Read the json from the file into a string
            jsonData = File.ReadAllText(filePath);
            N = SimpleJSON.JSON.Parse(jsonData);
        }
        else
        {
            Debug.LogError("Cannot load game data!");
        }
    }

    public int getNoofMissionItem()
    {
        JSONClass MissionJson = (JSONClass)N;
        return MissionJson.Count;
    }

    public Mission[] GetMissionValue()
    {
        JSONClass MissionJson = (JSONClass)N;
        Mission[] mission = new Mission[MissionJson.Count];

        for (int i = 0; i < MissionJson.Count; i++)
        {
            mission[i] = new Mission();
            mission[i].Name = MissionJson[i]["Name"].ToString();
            mission[i].Icon_Name = MissionJson[i]["Icon_Name"].ToString();
            mission[i].InitialValue = MissionJson[i]["InitialValue"].AsInt;
            mission[i].incrementalValue = MissionJson[i]["incrementalValue"].AsInt;
            mission[i].Coins = MissionJson[i]["Coins"].AsInt;
            mission[i].Mission_Message = MissionJson[i]["Mission_Message"];
        }

        return mission;
    }

    public void SetMissionValues(string name, int incrementalValue, string missionMsg)
    {
        JSONClass missionObject = (JSONClass)N;
        for (int i = 0; i < missionObject.Count; i++)
        {

            if (missionObject[i]["Name"].ToString().Replace("\"", "") == name)
            {
                Debug.Log("missionObject[i][Name] :" + missionObject[i]["Name"]);
                Debug.Log("Name :" + name + " IncValue :" + incrementalValue.ToString() + " meg :" + missionMsg);
                missionObject[i]["InitialValue"] = incrementalValue.ToString();
                missionObject[i]["Mission_Message"] = missionMsg.ToString();
                break;
            }
        }
        Debug.Log("New Value :" + missionObject.ToString());
        System.IO.File.WriteAllText(filePath, missionObject.ToString());
    }
}

public class WordHitsDigits
{
    public static string words_hitFileName = "Words_Hits.json";
    public static string jsonData = "";
    static SimpleJSON.JSONNode N;
    public static string filePath;
    public RootRandom rootRandom=null;

    public  WordHitsDigits()
    {
        rootRandom=new RootRandom();
        filePath = JsonToClass.getFullPath(words_hitFileName);
        if (File.Exists(filePath))
        {
            // Read the json from the file into a string
            jsonData = File.ReadAllText(filePath);
            N = SimpleJSON.JSON.Parse(jsonData);
            JSONArray hits=N["randomNO"].AsArray;
            rootRandom.randomNo=hits.ToString().Split(',');
            // Debug.Log("Length:"+ rootRandom.randomNo.Length.ToString());
        }
        else
        {
            rootRandom=null;
            Debug.LogError("Cannot load game data!");
        }
    }
}


[System.Serializable]
public class RootRandom
{
            public string[] randomNo; 
}








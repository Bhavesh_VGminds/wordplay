﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using VGMTools.Ads.Manager;
using VGMTools.Ads.Properties;
using VGMTools.Ads.Relations.Shared;
public class ShopDialog : Dialog
{

    public GameObject _IAPItemContaine, _IAPItemPrefab, _RewardButtonPrefab;
    public Color[] colors;
    public Text txtRewardText;


    protected override void Start()
    {
        base.Start();

        txtRewardText.text = string.Format(txtRewardText.text, ConfigController.instance.Config().admob.rewardedVideoAmount);

        if (!AdManager.IsAdReady<IRewardTag>())
        {
            AdManager.RequestAd<IRewardTag>();
        }

        CoinIAP[] iAPItem = IAPJsonClass.GetIAPData();
        IAPItem[] iapStoreItem = new IAPItem[iAPItem.Length];

        for (int i = 0; i < iAPItem.Length; i++)
        {
            GameObject item = Instantiate(_IAPItemPrefab, Vector3.one, Quaternion.identity) as GameObject;
            item.transform.SetParent(_IAPItemContaine.GetComponent<RectTransform>());
            item.transform.localScale = Vector3.one;

            //Image
            Transform CoinImageRoot = item.transform.Find("CoinImageBG");
            CoinImageRoot.GetComponent<Image>().color = new Color(colors[i].r, colors[i].g, colors[i].b, 255);
            if (iAPItem[i].ImageName != null)
            {
                CoinImageRoot.transform.GetComponentsInChildren<Image>()[1].sprite = Resources.Load(((iAPItem[i].ImageName != "NoAds") ? "CoinSetImages/" : "TagImages/") + iAPItem[i].ImageName, typeof(Sprite)) as Sprite;
                if (iAPItem[i].ImageName == "NoAds")
                {
                    CoinImageRoot.GetComponent<Image>().color = Color.white;
                    CoinImageRoot.transform.GetComponentsInChildren<Image>()[1].color = Color.red;
                }
            }

            //Tag
            Transform CoinTagImageRoot = CoinImageRoot.transform.Find("TagBG");
            // CoinTagImageRoot.GetComponent<Image>().color = new Color(colors[i + 1].r, colors[i + 1].g, colors[i + 1].b, 255);
            CoinTagImageRoot.GetComponent<Image>().color = Color.white;

            Debug.Log(iAPItem[i].Tag);
            if (iAPItem[i].Tag != "null")
            {
                CoinTagImageRoot.GetComponentsInChildren<Image>()[1].sprite = Resources.Load("TagImages/" + iAPItem[i].Tag, typeof(Sprite)) as Sprite;
            }
            else
            {
                CoinTagImageRoot.gameObject.SetActive(false);
            }

            item.transform.Find("txtCoins").GetComponent<Text>().text = ((iAPItem[i].Name != "No Ads") ? "x" : "") + iAPItem[i].Name;
            item.transform.Find("txtOffer").GetComponent<Text>().text = iAPItem[i].Offer;
            item.transform.Find("OldPrice").GetComponent<Text>().text = "$" + iAPItem[i].Old_Price.ToString();
            item.transform.Find("PriceBG").transform.Find("NewPrice").GetComponent<Text>().text = "$" + iAPItem[i].New_Price.ToString();

            if (i == iAPItem.Length - 1)
                item.transform.Find("Line").gameObject.SetActive(false);

            SetButton(item.transform.Find("PriceBG").GetComponent<Button>(), (i));
        }
    }

    void SetButton(Button button, int index)
    {
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(() => OnBuyProduct(index));
    }

    public void OnBuyProduct(int index)
    {
        Sound.instance.PlayButton();
        Purchaser.instance.BuyProduct(index);
    }

    public void WatchRewardAds()
    {
        AdManager.ShowAd<IRewardTag, System.Action>(CallBack);
    }

    void CallBack()
    {
        base.Close();
        DialogController.instance.ShowDialog(DialogType.RewardedVideo);
    }
}

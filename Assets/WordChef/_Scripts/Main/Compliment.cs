﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
public class Compliment : MonoBehaviour
{
    public Text txtCompliment;
    public Color[] color;
    public string[] Type;

    void Start()
    {
        Type = ConfigController.instance.GAME_CONFIG.appreciation_msg;
    }

    public void Show(int type)
    {
        txtCompliment.text = Type[type];
        TweenCompliment(txtCompliment.transform.parent);
    }

    public void ShowRandom()
    {
        txtCompliment.text = CUtils.GetRandom(Type);
        int index = Random.Range(0, color.Length);
        txtCompliment.transform.parent.GetComponent<Image>().color = new Color(color[index].r, color[index].g, color[index].b, 1);
        TweenCompliment(txtCompliment.transform.parent);
    }

    void TweenCompliment(Transform compliment)
    {
        compliment.DOMoveY(0, 0.5f).SetEase(Ease.Linear).From();
        compliment.GetComponent<Image>().DOFade(0.8f, 0.5f).OnComplete(() => TweenCallBack(compliment));
        txtCompliment.DOFade(0.8f, 0.5f);
    }

    private void TweenCallBack(Transform complimentText)
    {
        complimentText.DOKill(true);
        complimentText.GetComponent<Image>().DOFade(0, 0.5f).SetDelay(0.5f).OnComplete(() => TweenCallBack2(complimentText));
        txtCompliment.DOFade(0, 0.5f).SetDelay(0.5f);
    }

    private void TweenCallBack2(Transform complimentText2)
    {
        complimentText2.DOKill(true);
        complimentText2.localPosition = new Vector3(0, 245, 0);

    }
}

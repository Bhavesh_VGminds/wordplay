﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class RateUs : Dialog
{

    // Use this for initialization

    protected override void Start()
    {
        base.Start();
    }

    public void btnRate()
    {
        CUtils.RateGame();
        Prefs.IsRateUs = true;
        Close();
    }
}

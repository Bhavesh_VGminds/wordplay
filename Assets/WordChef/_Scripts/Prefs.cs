﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public static class Prefs
{

    public static Action<string, int> onMissionReached;

    public static int currentWorld
    {
        get { return CPlayerPrefs.GetInt("current_world", 0); }
        set { CPlayerPrefs.SetInt("current_world", ((value < 11) ? value : 11)); }
    }

    public static string currentWorldName
    {
        get { return CPlayerPrefs.GetString("current_world_Name", "Trainee"); }
        set { CPlayerPrefs.SetString("current_world_Name", value); }
    }

    public static int currentSubWorld
    {
        get { return CPlayerPrefs.GetInt("current_sub_world", 0); }
        set { CPlayerPrefs.SetInt("current_sub_world", value); }
    }

    public static int currentLevel
    {
        get { return CPlayerPrefs.GetInt("current_level", 0); }
        set { CPlayerPrefs.SetInt("current_level", value); }
    }

    public static int unlockedWorld
    {
        get { return CPlayerPrefs.GetInt("unlocked_world"); }
        set { CPlayerPrefs.SetInt("unlocked_world", ((value < 11) ? value : 777)); }
    }

    public static int unlockedSubWorld
    {
        get { return CPlayerPrefs.GetInt("unlocked_sub_world"); }
        set { CPlayerPrefs.SetInt("unlocked_sub_world", value); }
    }

    public static int unlockedLevel
    {
        get { return CPlayerPrefs.GetInt("unlocked_level"); }
        set
        {
            CPlayerPrefs.SetInt("unlocked_level", value);
            if (unlockedLevel != CPlayerPrefs.GetInt("unlocked_level"))
                TotalUplockedLevelAdded++;
        }
    }

    public static int[] GetPanWordIndexes(int world, int subWorld, int level)
    {
        return CryptoPlayerPrefsX.GetIntArray("pan_word_indexes_" + world + "_" + subWorld + "_" + level);
    }

    public static void SetPanWordIndexes(int world, int subWorld, int level, int[] indexes)
    {
        CryptoPlayerPrefsX.SetIntArray("pan_word_indexes_" + world + "_" + subWorld + "_" + level, indexes);
    }

    public static bool IsLastLevel()
    {
        return currentWorld == unlockedWorld && currentSubWorld == unlockedSubWorld && currentLevel == unlockedLevel;
    }

    public static void SetExtraWords(int world, int subWorld, int level, string[] extraWords)
    {
        CryptoPlayerPrefsX.SetStringArray("extra_words_" + world + "_" + subWorld + "_" + level, extraWords);
    }

    public static string[] GetExtraWords(int world, int subWorld, int level)
    {
        return CryptoPlayerPrefsX.GetStringArray("extra_words_" + world + "_" + subWorld + "_" + level);
    }

    public static int extraProgress
    {
        get { return CPlayerPrefs.GetInt("extra_progress", 0); }
        set { CPlayerPrefs.SetInt("extra_progress", value); }
    }

    public static bool isExtraWordOverlay
    {
        get { return CPlayerPrefs.GetBool("isExtraWordOverlay", false); }
        set { CPlayerPrefs.SetBool("isExtraWordOverlay", value); }
    }

    public static int extraTarget
    {
        get { return CPlayerPrefs.GetInt("extra_target", 10); }
        set { CPlayerPrefs.SetInt("extra_target", value); }
    }

    public static int totalExtraAdded
    {
        get { return CPlayerPrefs.GetInt("total_extra_added", 0); }
        set
        {
            CPlayerPrefs.SetInt("total_extra_added", value);
            if (onMissionReached != null && value != 0)
            {
                onMissionReached("Hidden_Words", CPlayerPrefs.GetInt("total_extra_added"));
            }
        }
    }

    public static int totalWordFindAdded
    {
        get { return CPlayerPrefs.GetInt("total_WordFind_added", 0); }
        set
        {
            Const.oldPoint = CPlayerPrefs.GetInt("total_WordFind_added", 0);
            if (Prefs.IsLastLevel())
                CPlayerPrefs.SetInt("total_WordFind_added", value);
            if (onMissionReached != null && value != 0)
            {
                onMissionReached("Words", CPlayerPrefs.GetInt("total_WordFind_added"));
            }
        }
    }

    public static string currentSubWorldName
    {
        get { return CPlayerPrefs.GetString("current_subworld_name", "Level"); }
        set { CPlayerPrefs.SetString("current_subworld_name", value); }
    }

    public static void AddToNumHint(int world, int subWorld, int level)
    {
        int numHint = GetNumHint(world, subWorld, level);
        PlayerPrefs.SetInt("numhint_used_" + world + "_" + subWorld + "_" + level, numHint + 1);
    }

    public static int GetNumHint(int world, int subWorld, int level)
    {
        return PlayerPrefs.GetInt("numhint_used_" + world + "_" + subWorld + "_" + level);
    }

    public static int totalHintAdded
    {
        get { return CPlayerPrefs.GetInt("total_Hint_added", 0); }
        set
        {
            CPlayerPrefs.SetInt("total_Hint_added", value);
            if (onMissionReached != null && value != 0)
            {
                onMissionReached("Hints", CPlayerPrefs.GetInt("total_Hint_added"));
            }
        }
    }
    public static int totalAdWatchAdded
    {
        get { return CPlayerPrefs.GetInt("total_AdWatch_added", 0); }
        set
        {
            CPlayerPrefs.SetInt("total_AdWatch_added", value);
            if (onMissionReached != null && value != 0)
            {
                onMissionReached("Reward_Video", CPlayerPrefs.GetInt("total_AdWatch_added"));
            }
        }
    }

    public static int totalPurchaseItemAdded
    {
        get { return CPlayerPrefs.GetInt("total_PurchaseItem_added", 0); }
        set
        {
            CPlayerPrefs.SetInt("total_PurchaseItem_added", value);

            if (onMissionReached != null && value != 0)
            {
                onMissionReached("Purchase_Item", CPlayerPrefs.GetInt("total_PurchaseItem_added"));
            }
        }
    }

    public static int GetTotalDaysOfLogin
    {
        get { return CPlayerPrefs.GetInt("total_Login_added", 0); }
        set
        {
            CPlayerPrefs.SetInt("total_Login_added", value);
            if (onMissionReached != null && value != 0)
            {
                onMissionReached("Login", CPlayerPrefs.GetInt("total_Login_added"));
            }
        }
    }

    public static int TotalUplockedLevelAdded
    {
        get { return CPlayerPrefs.GetInt("total_UnlockedLevel_added", 0); }
        set
        {
            CPlayerPrefs.SetInt("total_UnlockedLevel_added", value);
            if (onMissionReached != null && value != 0)
            {
                onMissionReached("Levels", CPlayerPrefs.GetInt("total_UnlockedLevel_added"));
            }
        }
    }

    public static string PlayerProfileImage
    {
        get { return CPlayerPrefs.GetString("PlayerProfileImage", "avatar1"); }
        set { CPlayerPrefs.SetString("PlayerProfileImage", value); }
    }

    public static int LastdayNumber
    {
        get { return CPlayerPrefs.GetInt("LastDayNumber", 0); }
        set { CPlayerPrefs.SetInt("LastDayNumber", value); }
    }

    public static bool IsAchivementActive
    {
        get { return CPlayerPrefs.GetBool("IsAchivementActive", false); }
        set { CPlayerPrefs.SetBool("IsAchivementActive", value); }
    }

    public static bool IsRateUs
    {
        get { return CPlayerPrefs.GetBool("IsRateUs", false); }
        set { CPlayerPrefs.SetBool("IsRateUs", value); }
    }

    public static bool IsSpecialCharOverlay
    {
        get { return CPlayerPrefs.GetBool("IsSpecialCharOverlay", false); }
        set { CPlayerPrefs.SetBool("IsSpecialCharOverlay", value); }
    }

    public static bool IsSpecialCellOverlay
    {
        get { return CPlayerPrefs.GetBool("IsSpecialCellOverlay", false); }
        set { CPlayerPrefs.SetBool("IsSpecialCellOverlay", value); }
    }


    public static int LastWorld
    {
        get { return CPlayerPrefs.GetInt("LastWorld", -1); }
        set { CPlayerPrefs.SetInt("LastWorld", value); }
    }

    public static bool IsMusicOn
    {
        get { return CPlayerPrefs.GetBool("IsMusicOn", true); }
        set { CPlayerPrefs.SetBool("IsMusicOn", value); }
    }

    public static bool IsSoundOn
    {
        get { return CPlayerPrefs.GetBool("IsSoundOn", true); }
        set { CPlayerPrefs.SetBool("IsSoundOn", value); }
    }

    public static int MLogin
    {
        get { return CPlayerPrefs.GetInt("MLogin", 7); }
        set { CPlayerPrefs.SetInt("MLogin", value); }
    }

    public static int MLevels
    {
        get { return CPlayerPrefs.GetInt("MLevels", 3); }
        set { CPlayerPrefs.SetInt("MLevels", value); }
    }

    public static int MWords
    {
        get { return CPlayerPrefs.GetInt("MWords", 50); }
        set { CPlayerPrefs.SetInt("MWords", value); }
    }

    public static int MHidden_Words
    {
        get { return CPlayerPrefs.GetInt("MHidden_Words", 15); }
        set { CPlayerPrefs.SetInt("MHidden_Words", value); }
    }

    public static int MHints
    {
        get { return CPlayerPrefs.GetInt("MHints", 10); }
        set { CPlayerPrefs.SetInt("MHints", value); }
    }

    public static int MReward_Video
    {
        get { return CPlayerPrefs.GetInt("MReward_Video", 3); }
        set { CPlayerPrefs.SetInt("MReward_Video", value); }
    }

    public static int MPurchase_Item
    {
        get { return CPlayerPrefs.GetInt("MPurchase_Item", 2); }
        set { CPlayerPrefs.SetInt("MPurchase_Item", value); }
    }

}

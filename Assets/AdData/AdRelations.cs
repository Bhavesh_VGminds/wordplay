﻿namespace VGMTools
{
	namespace Ads
	{
		namespace Relations
		{
			namespace Shared
			{
				public interface IBannerTag : IGenericAdTag
				{

				}

				public interface IInterstitialTag : IGenericAdTag
				{

				}

				public interface IRewardTag : IGenericAdTag
				{

				}

				public interface IVideoTag : IGenericAdTag
				{

				}	
			}
		}
	}
}
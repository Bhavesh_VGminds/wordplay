﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldItem : MonoBehaviour
{
    // public MaskableGraphic itemName, itemNumber, itemNumberBack;
    public Text itemName;
    public Image play, imgLevel;
    public Button button;
    public Sprite playUnactive, done;
    public Text processText, subWorldName;

    public int world, subWorld;

    private void Start()
    {
        Debug.Log("Name  :" + transform.parent.name);
        world = int.Parse(transform.parent.name);
        subWorld = transform.GetSiblingIndex();
        int numLevels = Superpow.Utils.GetNumLevels(world, subWorld);
        subWorldName.text = "Level " + (subWorld + 1).ToString();

        if (world > Prefs.unlockedWorld || (world == Prefs.unlockedWorld && subWorld > Prefs.unlockedSubWorld))
        {
            button.interactable = false;
            play.GetComponentsInChildren<Image>()[1].sprite = playUnactive;
            play.color = new Color32(110, 110, 110, 255);
            processText.text = "0" + "/" + numLevels;
            imgLevel.enabled = false;
        }
        else if (world == Prefs.unlockedWorld && subWorld == Prefs.unlockedSubWorld)
        {
            processText.text = Prefs.unlockedLevel + "/" + numLevels;
            imgLevel.enabled = false;
            DOTweenManager.ButtonActiveEffect(play.transform);
        }
        else
        {
            processText.text = "";
            imgLevel.sprite = done;
            imgLevel.enabled = true;

        }

        button.onClick.AddListener(OnButtonClick);
    }



    private void OnButtonClick()
    {
        Prefs.currentWorld = world;
        Prefs.currentSubWorld = subWorld;
        Prefs.currentSubWorldName = subWorldName.text;

        CUtils.LoadScene(3, true);
        Sound.instance.PlayButton();
    }
}

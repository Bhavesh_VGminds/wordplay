﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PauseDialog : Dialog
{

    public Transform PlayButton;

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start()
    {
        base.Start();
        PlayButton.GetComponent<Animator>().enabled = false;
        DOTweenManager.ButtonActiveEffect(PlayButton);
    }

    public void OnContinueClick()
    {
        Sound.instance.PlayButton();
        Close();
    }

    public void OnLevelClick()
    {
        Close();
    }


}

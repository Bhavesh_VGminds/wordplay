﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;


public class MyAppodeal : MonoBehaviour
{

    string appKey = "20518ad71f9366de0a94caf9e7cae1e8398e72e13a86c906";
    private string[] disbaledAppodealAdapters = new string[] {
        "amazon_ads",
        "chartboost",
        "unity_ads",
        "mailru",
        "adcolony",
        "yandex",
        "startapp",
        "avocarrot",
        "flurry",
        "cheetah",
        "inner-active",
        "revmob",
        "ogury"
    };


    void Start()
    {
        DisableAppodealAdapters();
        Appodeal.disableLocationPermissionCheck();
        Appodeal.setTesting(false);
        Appodeal.initialize(appKey, Appodeal.BANNER | Appodeal.INTERSTITIAL | Appodeal.REWARDED_VIDEO);
    }

    void DisableAppodealAdapters()
    {
        foreach (string str in disbaledAppodealAdapters)
        {
            Appodeal.disableNetwork(str);
        }
    }

}

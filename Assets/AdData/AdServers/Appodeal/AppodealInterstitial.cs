using System;
using UnityEngine;
using AppodealAds.Unity.Common;
using AppodealAds.Unity.Api;

namespace AdServers
{
	namespace AppodealAds
	{
		public class AppodealInterstitial : MonoBehaviour, IAppodealInterstitial,IInterstitialAdListener
		{
			private Action onAdComplete;

			void Start ()
			{
				Appodeal.setInterstitialCallbacks (this);
			}

			public void ShowAd (Action _OnAdComplete)
			{
				this.OnAdComplete = _OnAdComplete;
				Appodeal.show (Appodeal.INTERSTITIAL);
				Debug.Log ("ShowAdInt");
			}

			public Action OnAdComplete {
				get {
					return onAdComplete;
				}
				set {
					onAdComplete = value;
				}
			}

			public bool IsAdReady ()
			{
				return Appodeal.isLoaded (Appodeal.INTERSTITIAL);
			}

			public void OnAdFinished ()
			{
				Debug.Log ("OnAdClosed");
				if (OnAdComplete != null) {
					OnAdComplete.Invoke ();
				} else {
					Debug.Log ("not  found");
				}
			}

			#region IInterstitialAdListener implementation

			public void onInterstitialLoaded ()
			{
				Debug.Log ("Loaded");
			}

			public void onInterstitialFailedToLoad ()
			{
				
			}

			public void onInterstitialShown ()
			{
				
			}

			public void onInterstitialClosed ()
			{
				Debug.Log ("ClosedAD*****");
				OnAdFinished ();
			}

			public void onInterstitialClicked ()
			{
				
			}

            public void onInterstitialLoaded(bool isPrecache)
            {
                throw new NotImplementedException();
            }

            #endregion
        }
	}
}
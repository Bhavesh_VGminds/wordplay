﻿using UnityEngine;
using System;
using System.IO;

public class RootConfig
{
    public GameConfig gameConfig;
}

[System.Serializable]
public class GameVersion
{
    public  string ANDROID;
    public  string IOS ;
    public string WP;
}

[System.Serializable]
public class GameConfig
{
    public string androidPackageID;
    public string iosAppID;
    public string facebookPageID;
    public int extra_word_length;
    public int first_rate_level;
    public int show_share_stage;
    public int show_share_attempt;

    public int free_spin_wheel_frequency;

    public GameVersion versionConfig;
    public Admob admob;
    public Coins coinConfig;

    public string[] appreciation_msg;

    public Notification notificationConfig;
}

[System.Serializable]
public class Admob
{
    public int adPeriod;
    public int ad_interval_level;
    public int ad_interval_time;
    public int rewardedVideoPeriod;
    public int rewardedVideoAmount;
    public int banner_effect_time;
    public int reward_video_hint_show_level;
    public int banner_show_level;
    public int interstial_show_level;

    // [Header("Interstitial")]
    public string androidInterstitial;
    public string iosInterstitial;
    // [Header("Banner")]
    public string androidBanner;
    public string iosBanner;
    // [Header("RewardedVideo")]
    public string androidRewarded;
    public string iosRewarded;
}

[System.Serializable]
public class Notification
{
    public int notification_level;
    public int notification_first;
    public int notification_second;
}

[System.Serializable]
public class Coins
{
     public int beginner_pack_coins_limit;

    public int hint_cost_coin;
    public int word_cleared;
    public int level_cleared;
    public int extra_word_coin;

    public DailyCoins dailyCoinsConfig;
}
[System.Serializable]
public class DailyCoins{
    public int daily_login_coins_1;
    public int daily_login_coins_2;
    public int daily_login_coins_3;
    public int daily_login_coins_4;
    public int daily_login_coins_5;
    public int daily_login_coins_6;
    public int daily_login_coins_7;
}

public class Main_Config{
    public RootConfig Main_Config_Load(){
        string filePath =JsonToClass.getFullPath( "GameConfig.json");
        if (File.Exists(filePath))
        {
            // Debug.Log("File Found");
            // Read the json from the file into a string
           string gamedata = File.ReadAllText(filePath);
          return JsonUtility.FromJson<RootConfig>(gamedata);
        }
        else
        {
            Debug.LogError("Cannot load game data!");
           return null;
        }
    }

    


}




﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MedalsDialog : Dialog
{

    public SpriteRenderer MedalImage;

    protected override void Start()
    {
        base.Start();
        Sprite[] medalsSprites = Resources.LoadAll<Sprite>("Medals");
        MedalImage.sprite = medalsSprites.Single(s => s.name == Prefs.currentWorldName);
        Invoke("SoundPlay", 1);
    }

    void SoundPlay() { Sound.instance.Play(Sound.Others.WorldUp); }

}
